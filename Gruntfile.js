module.exports = function(grunt) {
    "use strict";
    var os = require('os'),
        osType = os.type();
    var PATHS = {
        js: [
            '*.json',
            '*.js',
            'src/**/*.json',
            'src/**/*.js'
        ]
    };
    var pkg = grunt.file.readJSON('package.json');
    var version = pkg.version;
    var short = pkg.short;
    var description = pkg.description;
    var campaign = pkg.compaignId;
    var option = grunt.file.readJSON('config/visicom/config.json');
    var name = option.name;
    var analytics = option.analytics;
    var target = grunt.option('target');
    var browser = grunt.option('browser');

    if (!browser || browser.length == 0)
        browser = 'chrome';

    // Project configuration.
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        clean: ['dist', 'temp', 'uglify'],
        jshint: {
            js: [
                "src/**/*.js"
            ],
            options: {
                esnext: true,
                reporter: require('jshint-summary'),
                ignores: ['Gruntfile.js', 'src/**/jquery*.js', 'src/**/lzma-d.js', 'src/**/piwik.lib.js'],
            },
            all: PATHS.js,
            watched: { src: [] }
        },
        jsonlint: {
            sample: {
                src: ['uglify/tracker_min.json'],
                options: {
                    formatter: 'prose'
                }
            }
        },
        sync: {
            chrome: {
                files: [
                    { cwd: 'src/shims/' + browser + '/', src: ['**', '!**/*.less', '!fonts/**/*.eot', '!fonts/**/*.svg', '!fonts/**/*.ttf', '!fonts/**/*.woff'], dest: 'dist/' + browser + '/', filter: 'isFile' },
                    { cwd: 'dist/temp/main/', src: ['**', '!**/*.less', '!fonts/**/*.eot', '!fonts/**/*.svg', '!fonts/**/*.ttf', '!fonts/**/*.woff'], dest: 'dist/' + browser + '/', filter: 'isFile' },
                ],
                updateAndDelete: false
            },
            firefox: {
                files: [
                    { cwd: 'src/shims/firefox/', src: ['**', '!**/*.less', '!fonts/**/*.eot', '!fonts/**/*.svg', '!fonts/**/*.ttf'], dest: 'dist/firefox/', filter: 'isFile' },
                    { cwd: 'dist/temp/main/', src: ['**', '!**/*.less', '!fonts/**/*.eot', '!fonts/**/*.svg', '!fonts/**/*.ttf'], dest: 'dist/firefox/data/', filter: 'isFile' },
                ],
                ignoreInDest: '**/*.rdf',
                updateAndDelete: false
            }
        },
        jpm: {
            options: {
                src: "./src/shims/firefox/",
                xpi: "./public/"
            }
        },
        copy: {
            main: {
                files: [
                    { expand: false, cwd: '', src: ['tracker.json'], dest: 'uglify/tracker_min.json', filter: 'isFile' },
                    { expand: false, cwd: '', src: ['tracker.json'], dest: 'uglify/tracker.json', filter: 'isFile' },
                    { expand: true, cwd: 'src/shims/' + browser + '/images/', src: ['**'], dest: 'temp/' + name + '/images/', filter: 'isFile' },
                    { expand: true, cwd: 'src/shims/' + browser + '/html/', src: ['**'], dest: 'temp/' + name + '/html/', filter: 'isFile' },
                    { expand: true, cwd: 'src/shims/' + browser + '/css/', src: ['**'], dest: 'temp/' + name + '/css/', filter: 'isFile' },
                    { expand: true, cwd: 'src/shims/' + browser + '/', src: ['manifest.json'], dest: 'temp/' + name + '/', filter: 'isFile' },
                ]
            }
        },
        uglify: {
            dev: {
                options: {
                    banner: '/*! ' + name + ' V<%= pkg.version %> (<%= grunt.template.today("yyyy-mm-dd") %>) */\n',
                    mangle: false,
                    compress: false,
                    beautify: true
                },
                files: [{
                        expand: true,
                        cwd: 'src/shims/' + browser + '/js',
                        src: ['*.js', '!main.js'],
                        dest: 'temp/' + name + '/js',
                    },
                    {
                        expand: true,
                        cwd: 'temp/' + name + '/js/',
                        src: ['main.js'],
                        dest: 'temp/' + name + '/js',
                    }
                ]
            },
            prod: {
                options: {
                    banner: '/*! ' + name + ' V<%= pkg.version %> (<%= grunt.template.today("yyyy-mm-dd") %>) */\n',
                    mangle: true,
                    compress: false,
                    beautify: false
                },
                files: [{
                        expand: true,
                        cwd: 'src/shims/' + browser + '/js',
                        src: ['*.js', '!main.js'],
                        dest: 'temp/' + name + '/js',
                    },
                    {
                        expand: true,
                        cwd: 'temp/' + name + '/js/',
                        src: ['main.js'],
                        dest: 'temp/' + name + '/js',
                    }

                ]
            }
        },
        zip: {
            chrome: {
                cwd: 'temp/' + name + '/',
                src: ['temp/' + name + '/**/*', ],
                dest: 'public/' + short + '_' + version + '.zip'
            },
            firefox: {
                cwd: 'temp/' + name + '/',
                src: ['temp/' + name + '/**/*', ],
                dest: 'public/' + short + '_' + version + '.xpi'
            }
        },
        'string-replace': {
            dist: {
                files: [{
                        expand: true,
                        cwd: 'src/shims/' + browser + '/',
                        src: ['**/manifest.json'],
                        dest: 'temp/' + name + '/'
                    },
                    {
                        expand: true,
                        cwd: 'src/shims/' + browser + '/js/',
                        src: ['**/main.js'],
                        dest: 'temp/' + name + '/js/'
                    },
                    {
                        expand: true,
                        cwd: 'uglify/',
                        src: ['tracker_min.json'],
                        dest: 'uglify/'
                    }
                ],
                options: {
                    replacements: [{
                            pattern: /__name__/g,
                            replacement: name
                        },
                        {
                            pattern: /__version__/g,
                            replacement: version
                        },
                        {
                            pattern: /__short__/g,
                            replacement: short
                        },
                        {
                            pattern: /__description__/g,
                            replacement: description
                        },
                        {
                            pattern: /__analytics__/g,
                            replacement: analytics
                        },
                        {
                            pattern: '__date__',
                            replacement: grunt.template.today("yyyy-mm-dd HH:MM")
                        }
                    ]
                }
            }
        },
        'regex-check': {
            files: "tracker.json",
            options: {
                pattern: /match/g
            },
        },
        'json-minify': {
            build: {
                files: 'uglify/tracker_min.json',
            }
        },
        'jsonmin': {
            dev: {
                options: {
                    stripWhitespace: true,
                    stripComments: true
                },
                files: {
                    "uglify/tracker.json": "uglify/tracker_min.json"
                }
            }
        },
        'jsObfuscate': {
            test: {
                options: {
                    concurrency: 2,
                    keepLinefeeds: false,
                    keepIndentations: false,
                    encodeStrings: true,
                    encodeNumbers: true,
                    moveStrings: true,
                    replaceNames: true,
                    variableExclusions: ['^_get_', '^_set_', '^_mtd_']
                },
                files: [{
                    expand: true,
                    cwd: 'src/shims/chrome/js',
                    src: ['*.js', '!piwik.lib.js', '!jquery-2.1.4.min.js'],
                    dest: 'temp/' + name + '/js'
                }]

            }
        },

        'exec': {
            tool: {
                command: 'brotli.exe --in uglify/tracker_min.json --out uglify/tracker.js.br --force'
            }
        }
    });


    // Load the plugins.
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-jsonlint');
    grunt.loadNpmTasks('grunt-sync');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-string-replace');
    grunt.loadNpmTasks('grunt-zip');
    grunt.loadNpmTasks('grunt-jpm');
    grunt.loadNpmTasks('grunt-regex-check');
    grunt.loadNpmTasks('grunt-json-minify');
    grunt.loadNpmTasks('grunt-jsonmin');
    grunt.loadNpmTasks('grunt-exec');
    grunt.loadNpmTasks('js-obfuscator');

    // Default task(s).
    grunt.registerTask('no-watch', ['clean', 'jshint:all', 'sync:' + browser]);
    grunt.registerTask('firefox', ['copy', 'string-replace', 'uglify:' + ((target === "prod") ? "prod" : "dev"), 'jsonlint', 'jsonmin', 'zip:firefox', 'json-minify']);
    grunt.registerTask('chrome', ['copy', 'string-replace', 'uglify:' + ((target === "prod") ? "prod" : "dev"), /*target == "prod" ? "jsObfuscate" : "jsonlint",*/ 'jsonlint', 'zip:chrome', 'jsonmin', 'json-minify', 'exec']);
    grunt.registerTask('default', ['no-watch', browser]);
};