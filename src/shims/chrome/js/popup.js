var domain;
var tab = {};
var options = {};

if (chrome.extension)
{
    var port = chrome.extension.connect({name: "Sample Communication"});
    port.onMessage.addListener(function(request) {
        //console.log("request:" + JSON.stringify(request));
        //console.log("request.message:" + request.message);
 	    if (!request.message ) return;
	    switch(request.message){
            case 'trackerresponse':
                //console.log("trackresponse:" + JSON.stringify(request));
                handleUI(request);
            break;
	    }
    });
}

function rank2Image( rank ) {
    var baseImage = './img/alexa/';
    var arank =
    
		[[0,'0']       ,  
		[100,'5']      ,
		[180,'44']     ,
		[320,'43']     ,
		[560,'42']     ,
		[1000,'41']    ,
		[1800,'4']     ,
		[3200,'33']    ,
		[5600,'32']    ,
		[10000,'31']   ,
		[18000,'3']    ,
		[32000,'22']   ,
		[56000,'21']   ,
		[100000,'2']   ,
		[180000,'11']  ,
		[320000,'1']   ,
		[560000,'01']  ,
		[1000000,'01']  
		];

    rank = 0+rank;
    for(i=0;i<arank.length;i++) {
	    if(rank<=arank[i][0]) {
		    return baseImage + arank[i][1] + '.png';
	    }
    }
    return baseImage + '0.png';
}

function onMessagePopFromBG (request, sender, sendResponse){
	if (!request.message ) return;
	switch(request.message){
        case 'trackerresponse':
            //console.log("trackresponse:" + JSON.stringify(request));
            handleUI(request);
        break;
	}
}

function produceHTML(label, cat) {
    var html = "";
    blockCount = 0;
    
    for (k=0; k< cat.length ;k++)
    {
         value = cat[k].value;
         title = cat[k].title;
         id = title.replace(" " , "_");
         block = cat[k].block;
         if (value === 1 && block === 0)
         {
             html += '<div class="bar"><div class="sub-cat">' + title + '</div>';
             html += '<div style="cursor: pointer;" id="' + label + '-' + id + '-allow"><div class="Line-small-off"></div><div class="small-slider knob-small-off"></div></div></div>';
            count++;
         }
         else  if (value === 1 && block === 1)
         {
             html += '<div class="bar"><div class="sub-cat">' + title + '</div>';
             html += '<div style="cursor: pointer;"  id="' + label + '-' + id + '-block"><div class="Line-small-on"></div><div class="small-slider knob-small-on"></div></div></div>';
            count++;
            blockCount++;
         }
         
    }

    return {html: html, count: count, blockCount: blockCount};
}


function populateCategories(item, options) {
//    console.log("populateCategories");
//    console.log("total:" + item.numberofresults + "|social:" + item.social + "|ad:" + item.ad + "||analytics:" + item.analytics);
    social_count = (item.social === 0) ? 0 : item.social;
    ad_count = (item.ad=== 0) ? 0 : item.ad;
    analytics_count = (item.analytics=== 0) ? 0 : item.analytics;
    //console.log("social_count=" + social_count);
    if (social_count > 0)
    {
        $("#window-social-head").html(" (" + social_count + ")");
        $('#social-category').addClass("enabledbutton");
        $('#social-category').addClass("enabledcursor");
        $('#window-social-expand').removeClass("disabledbutton");
    }else{
       $('#window-social-expand').addClass("disabledbutton");
    }
    
    if (ad_count > 0)
    {
        $("#window-ad-head").html(" (" + ad_count + ")");
       $('#ad-category').addClass("enabledbutton");
        $('#ad-category').addClass("enabledcursor");
        $('#window-ad-expand').removeClass("disabledbutton");
    }else{
         $('#window-ad-expand').addClass("disabledbutton");
    }
    
    if (analytics_count > 0)
    {
        $("#window-analytics-head").html(" (" + analytics_count + ")");
        $('#analytics-category').addClass("enabledbutton");
        $('#analytics-category').addClass("enabledcursor");
        $('#window-analytics-expand').removeClass("disabledbutton");
    }else{
        $('#window-analytics-expand').addClass("disabledbutton");
    }
    
    $social = item.$social;
    $analytics = item.$analytics;
    $ad = item.$ad;

    social = produceHTML("social", $social, $social.block);
    social_html  = social.html;
    
    $("#window-social").html(social_html);
    if (options.alexa === 0)
    {
         $("#alexa").css("visibility","hidden");
    }
    
    if (options.tracking.$social) {
         $('#window-social-global').html("Blocked");
         $("#window-social-global").unbind().click(function(e) {
            var optionsPage = chrome.extension.getURL('html/options.html');
            chrome.tabs.create({ url: optionsPage, active: true });
         });

         $('.dark-blue').addClass("disabledbutton");
    }else{
         $('#window-social-global').html("");
         $('.dark-blue').removeClass("disabledbutton");
    }
    $("#window-social-label, #window-social-expand").unbind().click(function(e) {
        if (social_count === 0)
            return;
        
        if ($("#window-social").css("display") === "none")
        {
            slidedown = parseInt(social_count * 35);
            $("#window-social-label").addClass('selectedcat');
            $("#window-ad-label").removeClass('selectedcat');
            $("#window-analytics-label").removeClass('selectedcat');
            $("#window-social-expand").attr("src", "./img/collapse.png");
            $("#window-ad-expand").attr("src", "./img/expand.png");
            $("#window-analytics-expand").attr("src", "./img/expand.png");
            $("#window-social").css("height", slidedown);
            $("#window-social").slideDown();
            $("#window-ad").slideUp();
            $("#window-analytics").slideUp();
       }else{
            $("#window-social-expand").attr("src", "./img/expand.png");
            $("#window-social-label").removeClass('selectedcat');
            $("#window-social").slideUp();
       }
    });
    ad = produceHTML("ad", $ad);
    ad_html  = ad.html;
    $("#window-ad").html(ad_html);
    
    if (options.tracking.$ad) {
         $('#window-ad-global').html("Blocked");
         $("#window-ad-global").unbind().click(function(e) {
            var optionsPage = chrome.extension.getURL('html/options.html');
            chrome.tabs.create({ url: optionsPage, active: true });
         });
         $('.dark-blue').addClass("disabledbutton");
    }else{
         $('#window-ad-global').html("");
         $('.dark-blue').removeClass("disabledbutton");
    }
    
    $("#window-ad-label, #window-ad-expand").unbind().click(function(e) {
        if (ad_count === 0)
            return;
        if ($("#window-ad").css("display") === "none")
        {
            slidedown = parseInt(ad_count * 35 - 10);
            $("#window-social-label").removeClass('selectedcat');
            $("#window-ad-label").addClass('selectedcat');
            $("#window-analytics-label").removeClass('selectedcat');
            $("#window-social-expand").attr("src", "./img/expand.png");
            $("#window-ad-expand").attr("src", "./img/collapse.png");
            $("#window-analytics-expand").attr("src", "./img/expand.png");
            $("#window-social").slideUp();
            $("#window-ad").css("height", slidedown);
            $("#window-ad").slideDown();
            $("#window-analytics").slideUp();
        }else{
            $("#window-ad-expand").attr("src", "./img/expand.png");
            $("#window-ad-label").removeClass('selectedcat');
            $("#window-ad").slideUp();
       }
    });
 
    analytics = produceHTML("analytics", $analytics);
    analytics_html  = analytics.html;
    $("#window-analytics").html(analytics_html);
   
     if (options.tracking.$analytics) {
         $('#window-analytics-global').html("Blocked");
         $("#window-analytics-global").unbind().click(function(e) {
            var optionsPage = chrome.extension.getURL('html/options.html');
            chrome.tabs.create({ url: optionsPage, active: true });
         });
         $('.dark-blue').addClass("disabledbutton");
    }else{
         $('#window-analytics-global').html("");
         $('.dark-blue').removeClass("disabledbutton");
    }
    $("#window-analytics-label, #window-analytics-expand").unbind().click(function(e) {
        if (analytics_count === 0)
            return;
        if ($("#window-analytics").css("display") === "none")
        {
            slidedown = parseInt(analytics_count * 32);
            $("#window-social-label").removeClass('selectedcat');
            $("#window-ad-label").removeClass('selectedcat');
            $("#window-analytics-label").addClass('selectedcat');
            $("#window-social-expand").attr("src", "./img/expand.png");
            $("#window-ad-expand").attr("src", "./img/expand.png");
            $("#window-analytics-expand").attr("src", "./img/collapse.png");
            $("#window-social").slideUp();
            $("#window-ad").slideUp();
            $("#window-analytics").css("height", slidedown);
            $("#window-analytics").slideDown();
        }else{
            $("#window-analytics").slideUp();
            $("#window-analytics-expand").attr("src", "./img/expand.png");
            $("#window-analytics-label").removeClass('selectedcat');
       }
    });
}


function handleUI(request) {
    try
    {
        //console.log("starts handleUI");
        //console.log("popup.onMessage:" + JSON.stringify(request));
        var online = request.online;
        var options = request.options;
        var item = request.tracker;
        var host = domain;
        
        /* for alexa */
        var rank = request.rank;
        var usrank = request.usrank | "";
        $("#alexa-rank").html(rank);

        //console.log("alexa host="+ host + "|rank=" + rank + "|online:" + online);
        var img = rank2Image(rank);
        //console.log("alexa img=" + img);
        $("#alexa-bar").attr("src", img);
        $("#alexa-bar").attr("title", "Global Rank:" + rank + "\n US Rank:" + usrank);
        $("#alexa-more-details").attr("title", "Global rank:" + rank + "\n US rank:" + usrank);
        $("#alexa-more-details").unbind().click(function(e) {
            var url = "http://www.alexa.com/siteinfo/" + host;
            chrome.tabs.create({ url: url, active: true });
        });
        $("#alexa-bar-link").unbind().click(function(e) {
            var url = "http://www.alexa.com/siteinfo/" + host;
            chrome.tabs.create({ url: url, active: true });
        });
      
        var details = {};
        if (chrome.browserAction) {
            chrome.browserAction.getBadgeBackgroundColor(details,function(colors) {
                var red = (parseInt(colors[0]) == 222); 
                //console.log("colors:" + colors + "|online:" + online + "|red=" + red);
                if(red)
                {
                    showStatus(2);
                }
                else
                if (!online)
                {
                    
                    showStatus(0);
                }
                else
                {
                    showStatus(1);
                }
            });
        }
        if (online)
        {
            /** enable the slider button */
            $("#slider-site").removeClass("disabledbutton");
            $('#slider-all').removeClass("disabledbutton");
            $('.global-text').removeClass("disabledbutton");
        }
        
        /** set the counter in the circle */
        count = item.numberofresults;
        $('#window-counter').html(count);

        populateCategories(item, options);
        
        var blockall = item.blockall;
        var blocksite = item.blocksite;
        //console.log("131.blockall:" + blockall + "|blocksite:" + blocksite);

        if (blocksite === 1)
        {
	        $('#slider-site').removeClass('knob-big-off');
	        $('#line-site').removeClass('line-big-off');
	        $('#slider-site').addClass('knob-big-on');
	        $('#line-site').addClass('line-big-on');
        }
        
        if (options.blockall === 1)
        {
	        $('#slider-site-div').addClass("disabledbutton");
	        $('#slider-site').addClass("disabledbutton");
	        $('#line-site').addClass("disabledbutton");
	        $('#block-all').prop('checked', true);
	        $('#block-all').attr('value', 1);
	        
	        $('#window-social .bar').addClass("disabledbutton");
	        $('#window-ad .bar').addClass("disabledbutton");
	        $('#window-analytics .bar').addClass("disabledbutton");
        }else{
	        $('#slider-site-div').removeClass("disabledbutton");
 	        $('#slider-site').removeClass("disabledbutton");
	        $('#line-site').removeClass("disabledbutton");
	        $('#block-all').prop('checked', false);
  	        $('#block-all').attr('value', 0);
	        
	        $('#window-social  .bar').removeClass("disabledbutton");
	        $('#window-ad .bar').removeClass("disabledbutton");
	        $('#window-analytics .bar').removeClass("disabledbutton");
        }
       
        var showBar = function()
        {
            $('.scrollbar').css("overflow-y", "scroll");
        };

        var hideBar = function()
        {
           // $('.scrollbar').css("overflow", "");
        };

        $('.scrollbar').hover(function(){
                    showBar();
                }, function(){
                    hideBar();
                });

        $(".bar").hover(function(){
            $(this).addClass('hover-bar');
        }, function(){
           $(this).removeClass('hover-bar');
        });
                
         if (!online) {
            $(".small-slider").addClass("disabledbutton");
        }
        
        $(".small-slider").unbind().click(function(e) {
            var target= e.target;
            var parent = target.parentNode;
            if (parent !== null)
            {
                var id = parent.id;
                if (id === "")
                    id = parent.parentNode.id;
                cat = "$" + id.split("-")[0];
                sub = id.split("-")[1].replace("_", " ");
                block = id.split("-")[2] == "block" ? 0 : 1;
                //console.log("cat:" + cat + "|sub:" + sub + "|block:" + block);
                port.postMessage( {message: "blockfeature", tab : tab.id, cat: cat , sub: sub, block: block , url:  tab.domain});
                chrome.tabs.sendMessage(tab.id, {message: "blockfeature", tab : tab.id, cat: cat , sub: sub, block: block , url:  tab.domain});
            }
        });
       
        //console.log("ends handleUI");
    }catch(e){
        console.log("error:" + e.message);
    }
}

function showStatus(status) {
    //console.log("status=" + status);
    switch(status)
    {
        case 0:
            $('#top-bar').removeClass('green');
            $('#top-bar').removeClass('red');
            $('#top-bar').addClass('gray');
            $('#domain-status').removeClass('green');
            $('#domain-status').removeClass('red');
            $('#domain-status').addClass('gray');
            $('#domain-status0').attr("src", "./img/shape.png");
            $('#domain-status-border').css('border-color', '#25607e');
            $('#domain-status1').html('OFFLINE');
            $('#domain-status1').css('width', '85px');
            $('#state').removeClass('Orb-Red');
            $('#state').addClass('Orb');
            $('.Tick').css('left', '65px');
            $('.Tick').css('top', '2px');
        break;
        case 1:
            $('#top-bar').removeClass('gray');
            $('#top-bar').removeClass('red');
            $('#top-bar').addClass('green');
            $('#domain-status').removeClass('gray');
            $('#domain-status').removeClass('red');
            $('#domain-status').addClass('green');
            $('#domain-status-border').css('border-color', '#5ADE35');
            $('#domain-status0').attr("src", "./img/tick.png");
            $('#domain-status1').html('SAFE');
            $('#domain-status1').css('width', '50px');
            $('#domain-status1').css('padding-left', '5px');
            $('#state').removeClass('Orb-Red');
            $('#state').addClass('Orb');
            $('.Tick').css('left', '55px');
            $('.Tick').css('top', '4px');
        break;
        case 2:
            $('#top-bar').removeClass('gray');
            $('#top-bar').removeClass('green');
            $('#top-bar').addClass('red');
            $('#domain-status').removeClass('gray');
            $('#domain-status').removeClass('green');
            $('#domain-status0').attr("src", "./img/shape.png");
            $('#domain-status-border').css('border-color', '#C62130');
            $('#domain-status1').html('UNSAFE');
            $('#domain-status').addClass('red');
            $('#domain-status1').css('width', '85px');
            $('#state').removeClass('Orb');
            $('#state').addClass('Orb-Red');
            $('.Tick').css('left', '65px');
            $('.Tick').css('top', '2px');
        break;
    }
}

function onLoad() {
	try
	{
	    if (chrome.runtime)
	    {
	        chrome.runtime.sendMessage({directive: "popup-click"}, function(response) {});
	    }
	}catch(e){}
	
	//console.log("onload starts");
    //myshield.$analytics.trackPage("Popup", "/popup.html");
    window.addEventListener("contextmenu", function(e) { e.preventDefault(); });
    showStatus(0);
	if (chrome.tabs)
	{
	    chrome.tabs.getSelected(null, function (request){
            console.log("starts.onload.getSelected.request.id:" + request.id + "|url:" + request.url);
            domain = myshield.$api.cleanUrl(request.url) ;
            $("#window-domain").html(domain);
            
            /* assignement important do not remove */
            tab = request;
            tab.domain = domain;
            
            port.postMessage({message: "gettrackers", url: domain});
            
            $("#window-options").unbind().click(function(e) {
                var optionsPage = chrome.extension.getURL('html/options.html');
                chrome.tabs.create({ url: optionsPage, active: true });
            });

	        $('#slider-site-div').unbind().click(function(e) {
	            if($('#slider-site').attr('class').indexOf('off') !== -1){
	                $('#slider-site').removeClass('knob-big-off');
	                $('#line-site').removeClass('line-big-off');
	                $('#slider-site').addClass('knob-big-on');
	                $('#line-site').addClass('line-big-on');
	                block = 1;
	            }else{
	                $('#slider-site').removeClass('knob-big-on');
	                $('#line-site').removeClass('line-big-on');
	                $('#slider-site').addClass('knob-big-off');
	                $('#line-site').addClass('line-big-off');
	                block = 0;
	            }
                port.postMessage({message: "blocksite", tab : request.id, block: block , url:  domain});
	        });

	        $('#block-all').unbind().click(function(e) {
	            var n = $( "input:checked" ).length;
	            if(n===1){
	                block = 1;
                }else{
	                block = 0;
                }
                port.postMessage({message: "blockall", tab : request.id, block: block , url:  domain});
	        });
        });
    }

    if (chrome.extension)
        chrome.extension.onMessage.addListener(onMessagePopFromBG);
	//console.log("onload ends");
}

document.addEventListener("DOMContentLoaded", onLoad, false);
