if (!myshield)
    var myshield = {};
myshield.$contentScript = (function() {
	var _this = {};

	// messages coming from iframes and the current webpage
	function onMessageFromDOM (event) {
		if (!event.data.message) return;
		//console.log("onMessageFromDOM:" + JSON.stringify(event.data.message));
		_this.sendMessage(event.data, null, null);
	}

	// messages coming from "background.js"
	function onMessageFromBG (request, sender, sendResponse) {
		//console.log("onMessageFromBG:" + JSON.stringify(request.message));
		_this.processMessage(request, sender, sendResponse);
	}

	_this.init = function() {
		// listen to the messages from the iframe
		//console.log("content.init");
		window.removeEventListener("message", onMessageFromDOM, false);
		window.addEventListener("message", onMessageFromDOM, false);

		// listen to messages from background
		chrome.extension.onMessage.removeListener(onMessageFromBG);
		chrome.extension.onMessage.addListener(onMessageFromBG);
	};

	_this.sendMessage = function(request, sender, sendResponse) {
		//console.log("sendMessage: Content script on message:" + request.message);
        chrome.runtime.sendMessage(request);
	};

	_this.processMessage = function(request, sender, sendResponse) {
		//console.log("Content script on message:" + request.message);
    	switch(request.message) {
    	    case 'getranking':
    	    case 'alexaranking':
            case 'gettrackers':
            case 'blockall':
            case 'blockfeature':
            case 'blocksite':
            case 'blockallfeature':
            case 'savesettings':
            case 'getoptions':
            case 'trackerresponse':
            case 'optionsresponse':
            case 'scanresponse':
            case 'removeallscripts':
               // console.log("processMessage:" + JSON.stringify(request));
                chrome.runtime.sendMessage(request);
            break;
        }
    };
   	return _this;

}());

myshield.$contentScript.init();
