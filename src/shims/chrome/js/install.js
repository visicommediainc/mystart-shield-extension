if (chrome.extension)
{
    var port = chrome.extension.connect({name: "Sample Communication"});
    port.onMessage.addListener(function(request) {
        //console.log("request:" + JSON.stringify(request));
    });
}

function onLoad() {
    $("#alexa").unbind().click(function(e) {
        target = $(e.target);
        if (target.attr('class').indexOf("checkbox-checked") === -1){
            target.removeClass("checkbox");
            target.addClass("checkbox-checked");
        }else{
            target.addClass("checkbox");
            target.removeClass("checkbox-checked");
        }
    });
    
    $(".More-Settings").unbind().click(function(e) {
        var alexa = $("#alexa").attr("class").indexOf("checkbox-checked") === -1 ? 0 : 1;
        var options =  {
                       alexa : alexa,
                       antiphishing : 1,
                       blockall: 0,
                       search : {
                            google : 1,
                            yahoo : 1,
                            bing : 1
                        },
                        tracking : {
                            $ad: 0,
                            $analytics: 0,
                            $social:0,
                            $sponsors:0,
                            $spyware:1
                        }
                    };
  		myshield.$settings.setOptions(options);
  		port.postMessage({message: "savesettings", options: options});
        var optionsPage = chrome.extension.getURL('html/options.html?alexa=' + alexa);
        window.location = optionsPage;
    });
    
    $("#hover-bar").unbind().click(function(e) {
        var alexa = $("#alexa").attr("class").indexOf("checkbox-checked") === -1 ? 0 : 1;
        var options =  {
                       alexa : alexa,
                       antiphishing : 1,
                       blockall: 0,
                       search : {
                            google : 1,
                            yahoo : 1,
                            bing : 1
                        },
                        tracking : {
                            $ad: 0,
                            $analytics: 0,
                            $social:0,
                            $sponsors:0,
                            $spyware:1
                        }
                    };
  		myshield.$settings.setOptions(options);
  		port.postMessage({message: "savesettings", options: options});
        var url = "http://mystartshield.com";
        window.location.href = url ;
    });
    window.addEventListener("contextmenu", function(e) { e.preventDefault(); });
}

document.addEventListener("DOMContentLoaded", onLoad, false);
