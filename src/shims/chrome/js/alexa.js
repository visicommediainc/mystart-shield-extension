myshield.$alexa = {
    rankers : {},
    us_rankers : {},
    barTimeId : "",
 	baseline: "https://xml.alexa.com/data?cli=10&dat=nsa&ver=visicom-myshield",
 	getRemoteInfo : function(url, callback, type) {
        var xhr = new XMLHttpRequest();
        xhr.onload = function() {
            if (type === "arraybuffer")
            {
                var arrayBuffer = xhr.response;
                if (arrayBuffer) {
                    var byteArray = new Uint8Array(arrayBuffer);
                    callback(myshield.$tracker.bin2String(byteArray));
                    arrayBuffer = null;
                    byteArray = null;
                    xhr = null;
                }
            }else{
                callback(xhr.responseText);
                xhr = null;
            }
        };
        if (type === "arraybuffer")
          xhr.responseType = "arraybuffer";

        xhr.open("GET", url, true);
        xhr.send();
    },
    $init: function() {
        function  addZero(value){
            if (parseInt(value) < 10)
                value = '0' + value;
            return value;
        }
        
        Date.prototype.timestamp = function() {
            var mm = addZero(this.getMonth() + 1); // getMonth() is zero-based
            var dd = addZero(this.getDate());
            var h = addZero(this.getHours());
            var m = addZero(this.getMinutes());
            var s = addZero(this.getSeconds());            

          return [this.getFullYear(), mm, dd , h, m, s].join(''); // padding
        };

        if (myshield.$alexa.barTimeId === "") {
            myshield.$api.getValue("alexats", function (result) {
                if (result.alexats)
                    myshield.$alexa.barTimeId = result.alexats;
                else
                {    
                    var date = new Date();
                    timestamp = date.timestamp();
                    console.log("timestamp:" + timestamp);
                    myshield.$alexa.barTimeId = timestamp;
                    chrome.storage.local.set({"alexats": timestamp});
                }
           });
        }
    },
    parseXml : function( str ) {
		var xmlDoc = null;
		if(typeof (window.DOMParser) != "undefined") {	
			parser=new DOMParser();
			xmlDoc=parser.parseFromString(str,"text/xml");
		}
		else {// Internet Explorer
		
			xmlDoc=this.createXmlObject();
			xmlDoc.async=false;
			xmlDoc.loadXML(str); 
			xmlDoc.setProperty("SelectionLanguage", "XPath");
		}
		return xmlDoc;
	},
    getXPathText : function(xmlText, xpath) {
		var xmldoc = myshield.$alexa.parseXml(xmlText);
		try {
			return xmldoc.evaluate(xpath,xmldoc, null, XPathResult.STRING_TYPE, null).stringValue;
		}
		catch(e) {
			try {
				return xmldoc.documentElement.selectSingleNode(xpath).value;
			}
			catch(e2) {}
		}
		return "";
	},
	getAlexa : function(host, tab) {
        if (myshield.$settings.options.alexa === 0 || host === "")
            return;
        if (typeof myshield.$alexa.rankers[host] !==  'undefined')
        {
            return;
        }
        
	    var xmlurl=myshield.$alexa.baseline + "&uid=" + myshield.$alexa.barTimeId + "&url=" + host ;
	    console.log("xmlurl=" + xmlurl);
        myshield.$alexa.getRemoteInfo(xmlurl, function(xmlText){
            var rank = myshield.$alexa.getXPathText(xmlText,"//SD/POPULARITY/@TEXT");
            var us_rank = myshield.$alexa.getXPathText(xmlText,"//SD/COUNTRY /@RANK");
            myshield.$alexa.rankers[host] = rank;
            if (us_rank !== "")
                myshield.$alexa.us_rankers[host] = us_rank;
           console.log("host=" + host + "|rank=" + rank + "|us.rank="+ us_rank);
        });
	},
	onCommitted : function(tab) {
        if (myshield.$settings.options.alexa === 0)
            return;
        var isMainFrame = (tab.frameId === 0) ;
		if(!tab || !myshield.$api.checkUrl(tab.url) || !isMainFrame)
        {
            return;
        }
        var host = myshield.$api.cleanUrl(tab.url);
        myshield.$alexa.getAlexa(host, tab);
    },
    onSelectionChanged: function(tabId, changeInfo) {
        if (myshield.$settings.options.alexa === 0)
            return;
	    chrome.tabs.get(tabId, function(tab) {
 			if(!tab || !myshield.$api.checkUrl(tab.url))
            {
               myshield.$tracker.updateButton(0);
                chrome.browserAction.setPopup({
                    popup: ""
                });
                return;
            }
            var host = myshield.$api.cleanUrl(tab.url);
            myshield.$alexa.getAlexa(host, tab);
        });
    }
};

document.addEventListener("DOMContentLoaded", function() {
    myshield.$alexa.$init();
    chrome.tabs.onSelectionChanged.addListener( myshield.$alexa.onSelectionChanged );
    chrome.webNavigation.onCommitted.addListener(myshield.$alexa.onCommitted);
}, false);