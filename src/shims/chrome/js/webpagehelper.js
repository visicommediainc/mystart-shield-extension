(function() {
	var manifest = chrome.runtime.getManifest();
	var node = document.createElement("script");

	// Extension's attributes
	node.setAttribute("data-extension",   "installed");
	node.setAttribute("data-id",          chrome.runtime.id);
	node.setAttribute("data-version",     manifest.version);
	node.setAttribute("data-name",        manifest.name);

	document.documentElement.insertBefore(node, null);
})();
