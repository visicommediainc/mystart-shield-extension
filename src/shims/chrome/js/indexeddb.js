myshield.$db = {
    indexedDB : null,
    IDBKeyRange : null,
    openCopy : null,
    IDBTransaction: null,
    indexedDBOk : function indexedDBOk() {
        return "indexedDB" in window;
    },
    setData : function(key, data) {
            var idb = myshield.$db.indexedDB;
            if (!idb.objectStoreNames.contains('mwlist'))
            {
                trans = idb.createObjectStore('mwlist');
            }
            else
            {
                trans = idb.transaction([ "mwlist" ], this.IDBTransaction.READ_WRITE);
            }
             var store = trans.objectStore('mwlist');

            // add
            var requestAdd = store.put(data, key);
            requestAdd.onsuccess = function(e) {
                console.log("indexedDB saving success");
            };

            requestAdd.onfailure = function(e) {
                console.log("indexedDB saving failure");
            };
         },
    getData : function(key, callback) {
        try
        {
            var idb = myshield.$db.indexedDB;
            var transaction = idb.transaction('mwlist', IDBTransaction.READ_ONLY);
            var objectStore = transaction.objectStore('mwlist');
            var request = objectStore.get(key);
            request.onsuccess = function(e) {
                var result = e.target.result;
                callback(result);
                result = null;
                return;
            };
         }catch(e){
            callback(null);
         }
     },
    getResult : function(key, callback) {
        idb = myshield.$db.indexedDB;
        var transaction = idb.transaction('mwlist', IDBTransaction.READ_ONLY);
        var objectStore = transaction.objectStore('mwlist');
        var request = objectStore.get("mwlist");
        request.onsuccess = function(event) {
            var result = event.target.result;
            callback(key, result[key]);
            result = null;
        };
    }
};


document.addEventListener("DOMContentLoaded", function() {
    myshield.$db.indexedDB = window.indexedDB || window.webkitIndexedDB || window.msIndexedDB;
    myshield.$db.IDBKeyRange = window.IDBKeyRange || window.webkitIDBKeyRange;
    myshield.$db.openCopy = indexedDB && indexedDB.open;
    myshield.$db.IDBTransaction = window.IDBTransaction || window.webkitIDBTransaction;
    /***
     * Remove database snippet
    * */
    var dropDatabase = function(name)
    {
        var request = indexedDB.deleteDatabase(name);
        request.onsuccess = function() {
            /* drop succeeded */
            console.log("indexedDB drop succeeded");
        };
        request.onerror = function() {
            /* drop failed */
            console.log("indexedDB drop failed");
        };
    };



    if (myshield.$db.IDBTransaction)
    {
        myshield.$db.IDBTransaction.READ_WRITE = myshield.$db.IDBTransaction.READ_WRITE || 'readwrite';
        myshield.$db.IDBTransaction.READ_ONLY = myshield.$db.IDBTransaction.READ_ONLY || 'readonly';
    }
     /* Create database snippet
     * */
    request = myshield.$db.indexedDB.open('id_myshield');

    request.onupgradeneeded = function(e)
    {
        //console.log("running onupgradeneeded");
        var thisDB = e.target.result;

        if(!thisDB.objectStoreNames.contains("mwlist")) {
            var store = thisDB.createObjectStore("mwlist"); //,{keyPath: 'url', autoIncrement: true});
            //store.createIndex('by_mwlist', 'url', {unique: true, multiEntry: false});
        }
    };
    request.onsuccess = function(e) {
        myshield.$db.indexedDB = e.target.result;
        //console.log("indexedDB running success");

        // dropDatabase('id_myshield');
        myshield.$parser.getFeed();
    };

    request.onerror = function(e) {
        //Do something for the error
         console.log("indexedDB running error");
   };


},false);
