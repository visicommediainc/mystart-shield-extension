(function() {
    if (window) {
        delete window.WebSocket;
        if ('WebSocket' in window) {
            window.WebSocket = undefined;
        }
        //console.log('WebSocket support disabled');
    }
    if (
        window.WebSocket instanceof Function === false ||
        window.WeakMap instanceof Function === false
    ) {
        return false;
    }
}());

var domLoaded = function(ev) {
    for (var idx in document.all)
    {
        var elm = document.all[idx];
        if (elm.tagName === "SCRIPT" && elm.innerText !== "")
        {
            if (elm.outerHTML.indexOf("wss://") !== -1 ||
                elm.outerHTML.indexOf("ws://") !== -1  ||
                elm.outerHTML.indexOf("\"wss\"") !== -1 ||
                elm.outerHTML.indexOf("\"ws\"") !== -1 
            ) 
            {
               elm.outerHTML = "<script></script>";
               elm.parentNode.removeChild(elm);
               document.all[idx] = null;
               delete document.all[idx];
           }
        }
    }
};

document.addEventListener('DOMContentLoaded', domLoaded);
