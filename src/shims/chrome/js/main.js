myshield.$tracker = {
    debug: false,
    activeTracker: {},
    activeTabId: 0,
    activeTabUrl: "",
    deferTab: -1,
    openerTab: -1,
    blockHost: "mystartshield.com/extensions/blocked.html",
    bin2String: function(array) {
        var result = "";
        for (var i = 0; i < array.length; i++) {
            result += String.fromCharCode(parseInt(array[i]));
        }
        return result;
    },
    getRemoteInfo: function(url, callback, type) {
        var xhr = new XMLHttpRequest();
        xhr.onload = function() {
            if (type === "arraybuffer") {
                var arrayBuffer = xhr.response;
                if (arrayBuffer) {
                    var byteArray = new Uint8Array(arrayBuffer);
                    callback(myshield.$tracker.bin2String(byteArray));
                    arrayBuffer = null;
                    byteArray = null;
                    xhr = null;
                }
            } else {
                callback(xhr.responseText);
                xhr = null;
            }
        };
        if (type === "arraybuffer")
            xhr.responseType = "arraybuffer";

        xhr.open("GET", url, true);
        xhr.send();
    },
    isBlockingPage: function(url) {
        if (!myshield.$settings.options.antiphishing || myshield.$settings.options.antiphishing === 0 || typeof url === 'undefined')
            return false;

        var UNSAFE_URI_RE = /(?=mystartshield.com)(?=.*blocked.html)/gi;
        if (url && url.match(UNSAFE_URI_RE)) {
            return true;
        }

        if (url.indexOf(myshield.$tracker.blockHost) !== -1) {
            return true;
        } else {
            return false;
        }
    },
    initTrack: function(item) {
        for (var cat in item) {
            item[cat].itemcount = 0;
            for (i = 0; i < item[cat].length; i++) {
                if (!item[cat][i].value)
                    item[cat][i].value = 0;
                if (!item[cat][i].block)
                    item[cat][i].block = 0;
            }
        }
        item.blockall = myshield.$settings.options.blockall;
        item.social = 0;
        item.ad = 0;
        item.analytics = 0;
        item.markedBlock = 0;
        item.blocksite = 0;
        item.numberofresults = 0;
        item.previousresults = 0;
        item.active = 0;
        item.blocked = 0;
        //console.log("initTracker:" + JSON.stringify(item));
        return item;
    },
    urlInfoChange: function(tab) {
        //console.log("urlInfoChange." + JSON.stringify(tab));
        var item = jQuery.extend(true, {}, myshield.$api.tracker);
        item = myshield.$tracker.initTrack(item);
        var url = myshield.$api.cleanUrl(tab.url);
        item.url = url;
        var status = 1;

        var itemMatched = myshield.$tracker.isMatched(tab.url, tab.id);
        if (itemMatched !== null) {
            cat = itemMatched.cat;
            if (cat === "$spyware") {
                item.markedBlock = 1;
                myshield.$tracker.updateButton(2);
                chrome.tabs.sendMessage(tab.id, { message: "removeallscripts" });
                if (!myshield.$tracker.isBlockingPage(tab.url) &&
                    myshield.$settings.options.antiphishing === 1 &&
                    tab.url.indexOf("alwd=1") === -1) {
                    chrome.tabs.update(tab.id, {
                        url: myshield.$events.UNSAFE_URL + myshield.$api.cleanUrl(tab.url)
                    });
                    item.active = 1;
                    myshield.$tracker.activeTracker = item;
                    console.log("98.redirection.url=" + url);
                    return;
                }
            }
        }

        if (tab.url !== null && myshield.$tracker.isBlockingPage(tab.url)) {
            item.markedBlock = 1;
            myshield.$tracker.updateButton(2);
            status = 2;
        } else if (!myshield.$api.checkUrl(url)) {
            status = 0;
        }
        var trackers = myshield.$api.trackers.length;
        if (trackers === 0) {
            myshield.$api.trackers.push(item);
            //console.log("adding " + url);
        } else {
            jQuery.each(myshield.$api.trackers, function(i, val) {
                if (val.url === url) {
                    //console.log("urlInfoChange.found.url:" + url);
                    item = val;
                    return false;
                } else if (i == trackers - 1) {
                    myshield.$api.trackers.push(item);
                    //console.log("adding " + url);
                }
            });
        }
        myshield.$tracker.activeTracker = item; //myshield.$api.getTrack(url);
        myshield.$tracker.updateButton(status, item.numberofresults);
    },
    setAllBlock: function(block) {
        for (i = 0; i < myshield.$api.trackers.length; i++) {
            item = myshield.$api.trackers[i];
            for (var category in item) {
                var cat = item[category];
                if (category.indexOf("$") === -1)
                    continue;
                for (k = 0; k < cat.length; k++) {
                    cat[k].block = block;
                }
            }
            item.blockall = block;
            if (!item.numberofitems)
                item.numberofresults = 0;
        }
    },
    setTrack: function(url, tracker) {
        for (i = 0; i < myshield.$api.trackers.length; i++) {
            if (myshield.$api.trackers[i].url === url) {
                myshield.$api.trackers[i] = tracker;
            }
        }
        //        chrome.storage.local.set({"trackers": JSON.stringify(myshield.$api.trackers)}, function(){
        //            console.log("setTrack.myshield.$api.trackers has been updated");
        //        });
    },
    updateAllTrack: function(block, url, callback) {
        var item = myshield.$api.getTrack(url);
        myshield.$settings.options.blockall = block;
        this.setAllBlock(block);
        for (var category in item) {
            var cat = item[category];
            if (category.indexOf("$") === -1)
                continue;
            for (k = 0; k < cat.length; k++) {
                cat[k].block = block;
            }
        }
        //        chrome.storage.local.set({"trackers": JSON.stringify(myshield.$api.trackers)}, function(){
        //            console.log("setTrack.myshield.$api.trackers has been updated");
        //        });
        chrome.storage.local.set({ "blockall": myshield.$settings.options.blockall }, function() {
            console.log("setTrack.myshield.$settings.options.blockall to " + myshield.$settings.options.blockall);
        });
        myshield.$tracker.activeTracker = item;
        if (callback)
            callback(item);
    },
    updateTrackSite: function(url, block, callback) {
        item = myshield.$api.getTrack(url);
        if (item) {
            item.blocksite = block;
            for (var category in item) {
                var cat = item[category];
                if (category.indexOf("$") === -1)
                    continue;
                for (k = 0; k < cat.length; k++) {
                    cat[k].block = block;
                }
            }
        }
        if (callback)
            callback(item);
    },
    updateTrack: function(cat, sub, block, url, callback) {
        parent = myshield.$api.getTrack(url);
        item = myshield.$tracker.activeTracker[cat];
        if (item) {
            for (i = 0; i < item.length; i++) {
                if (item[i].title === sub) {
                    item[i].block = block;
                    break;
                }
            }
        }
        myshield.$tracker.activeTracker = parent;
        //        chrome.storage.local.set({"trackers": JSON.stringify(myshield.$api.trackers)}, function(){
        //            console.log("setTrack.myshield.$api.trackers has been updated");
        //        });
        if (callback)
            callback(parent);
    },
    updateCategory: function(cat, block, url, callback) {
        item = myshield.$tracker.activeTracker[cat];
        if (item) {
            for (i = 0; i < item.length; i++) {
                item[i].block = block;
            }
        }
        myshield.$tracker.activeTracker = item;
        if (callback)
            callback(item);
    },
    getCount: function(type) {
        count = 0;
        for (x = 0; x < type.length; x++) {
            value = type[x].value;
            if (value === 1) {
                count++;
            }
        }
        return count;
    },
    fixReg: function(match) {
        if (match.indexOf("RegExp") !== -1) {
            match = match.replace("RegExp(/", "");
            match = match.replace(new RegExp('\\)+$'), '');
            match = match.replace("/g", "");
            return new RegExp(match, "g");
        } else {
            return match;
        }
    },
    matchTrack: function(cat, request) {
        url = request.url;
        for (k = 0; k < cat.length; k++) {
            if (!cat[k].value)
                cat[k].value = 0;
            match = cat[k].match;

            if (url.search(myshield.$tracker.fixReg(match)) != -1 && myshield.$tracker.activeTabId === request.tabId) {
                cat[k].value = 1;
            }
        }
        return cat;
    },
    isMatched: function(url, tabId) {
        if (myshield.$tracker.isBlockingPage(url))
            return null;

        var item = myshield.$tracker.activeTracker;
        for (var category in item) {
            var cat = item[category];
            if (category.indexOf("$") === -1)
                continue;
            for (var k = 0; k < cat.length; k++) {
                match = cat[k].match;
                if (url.search(myshield.$tracker.fixReg(match)) != -1) {
                    //console.log("category:" + category + "|title:" + cat[k].title + "|url:" + url);
                    if (myshield.$tracker.activeTabId === tabId) {
                        cat[k].value = 1;
                    }
                    return { cat: category, item: cat, index: k };
                }
            }
        }
        return null;
    },
    getCurrentTab: function(callback) {
        chrome.tabs.getSelected(null, function(tab) {
            callback(tab);
        });
    },
    updateButton: function(state, text) {
        if (state === 0)
            text = "";
        if (typeof text !== 'undefined') {
            if (text === 0 || text === -1) text = "";
            if (myshield.$tracker.activeTracker.numberofresults > 0) text = myshield.$tracker.activeTracker.numberofresults;
            if (typeof(text) === 'number') text = String(text);
            chrome.browserAction.setBadgeText({ text: text });
        }
        switch (state) {
            case 0:
                chrome.browserAction.setIcon({ path: "../images/neutral-icon.png" });
                break;
            case 1:
                {
                    if (myshield.$tracker.activeTracker.numberofresults > 0 && myshield.$tracker.activeTracker.blocked === 1)
                        chrome.browserAction.setIcon({ path: "../images/shield-blue.png" });
                    else
                        chrome.browserAction.setIcon({ path: "../images/safe-icon.png" });

                    chrome.browserAction.setBadgeBackgroundColor({ color: "#48BA30" });
                }
                break;
            case 2:
                if (myshield.$tracker.activeTracker.numberofresults > 0 && myshield.$tracker.activeTracker.blocked === 1)
                    chrome.browserAction.setIcon({ path: "../images/shield-unsafe.png" });
                else
                    chrome.browserAction.setIcon({ path: "../images/unsafe-icon.png" });

                chrome.browserAction.setBadgeBackgroundColor({ color: "#DE3535" });
                break;
        }
    },
    onBeforeRequest: function(request) {
        var isMainFrame = request.type === "main_frame";

        if (!request || request.tabId === -1 || myshield.$tracker.deferTab !== -1 || !myshield.$api.checkUrl(request.url) || isMainFrame) {
            return { cancel: false };
        }

        var itemMatched = myshield.$tracker.isMatched(request.url, request.tabId);
        if (itemMatched !== null) {
            itemMatched.active = 1;
            myshield.$tracker.activeTracker.blocked = 0;
            var cat2 = itemMatched.cat;

            var catBlock = myshield.$settings.options.tracking[cat2] === 1;
            if (myshield.$tracker.activeTracker.blockall === 1 || itemMatched.item[itemMatched.index].block == 1 || catBlock) {
                //console.log("isBlocked.Block=" + request.url + "|tracker:" + itemMatched.item[itemMatched.index].title);
                myshield.$tracker.activeTracker.blocked = 1;
                if (request.type == 'script' || request.type == 'xmlhttprequest') {
                    return { cancel: true };
                } else
                if (request.type == 'sub_frame') {
                    return { redirectUrl: 'about:blank' };
                } else if (request.type == 'image') {
                    return {
                        redirectUrl: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAEAAAABCAYAAAAfFcSJAAAAFElEQVR4XgXAAQ0AAABAMP1L30IDCPwC/o5WcS4AAAAASUVORK5CYII='
                    };
                } else {
                    return { cancel: true };
                }
            }
        }
        var item = myshield.$tracker.activeTracker;
        if (item.url != myshield.$tracker.activeTabUrl ||
            request.tabId !== myshield.$tracker.activeTabId) {
            //console.log("%c url:" + item.url + " is different from " + myshield.$tracker.activeTabUrl, "color:#FF8F1C; font-size:12px;");
            return { cancel: false };
        }
        item.active = 1;
        $social = item.$social;
        $analytics = item.$analytics;
        $ad = item.$ad;
        if (!$social || !$analytics || !$ad) {
            return { cancel: false };
        }

        item.social = myshield.$tracker.getCount($social);
        item.analytics = myshield.$tracker.getCount($analytics);
        item.ad = myshield.$tracker.getCount($ad);
        item.numberofresults = item.social + item.analytics + item.ad;

        if ((item.url !== null && item.url === myshield.$tracker.activeTabUrl) &&
            request.tabId === myshield.$tracker.activeTabId) {
            if (myshield.$tracker.isBlockingPage(item.url) || item.markedBlock === 1) {
                myshield.$tracker.updateButton(2, item.numberofresults);
            } else {
                myshield.$tracker.updateButton(1, item.numberofresults);
            }
            //console.log("url=" + item.url + "|numberofresults=" + item.numberofresults); 
            item.previousresults = item.numberofresults;
            chrome.tabs.sendMessage(myshield.$tracker.activeTabId, {
                message: "trackerresponse",
                tracker: item,
                options: myshield.$settings.options,
                rank: myshield.$alexa.rankers[item.url],
                usrank: myshield.$alexa.us_rankers[item.url],
                online: true
            });
        }
        return { cancel: false };
    },
    onBeforeNavigate: function(request) {
        myshield.$tracker.openerTab = request.tabId;
        if (myshield.$tracker.isBlockingPage(request.url)) {
            myshield.$tracker.updateButton(2);
        }
        var itemMatched = myshield.$tracker.isMatched(request.url, request.tabId);
        if (itemMatched !== null) {
            cat = itemMatched.cat;
            if (cat === "$spyware") {
                myshield.$tracker.updateButton(2);
                chrome.tabs.sendMessage(request.tabId, { message: "removeallscripts" });
                if (!myshield.$tracker.isBlockingPage(request.url) &&
                    myshield.$settings.options.antiphishing === 1 &&
                    request.url.indexOf("alwd=1") === -1) {
                    chrome.tabs.getCurrent(function(tab) {
                        if (!tab)
                            return;
                        chrome.tabs.update(tab.id, {
                            url: myshield.$events.UNSAFE_URL + myshield.$api.cleanUrl(request.url)
                        });
                    });

                    url = myshield.$events.UNSAFE_URL + myshield.$api.cleanUrl(request.url);
                    console.log("447.redirection.url=" + url);
                    return { redirectUrl: url };
                }
            }
        }
    },
    onPopupMessage: function(port) {
        //console.log("Connected .....");
        port.onMessage.addListener(function(request) {
            //console.log("message recieved:"+ JSON.stringify(request));
            switch (request.message) {
                case 'savesettings':
                    {
                        console.log("savesettings:" + JSON.stringify(request));
                        if (request.options.antiphishing !== myshield.$settings.options.antiphishing) {
                            console.log("antiphishing options changed to " + request.options.antiphishing);
                            myshield.$parser.getFeed();
                        }
                        myshield.$settings.options = request.options;
                        chrome.storage.local.set({ "options": myshield.$settings.options }, function() {});
                    }
                    break;
                case 'gettrackers':
                    {
                        item = myshield.$api.getTrack(myshield.$api.cleanUrl(request.url));
                        if (!item)
                            item = myshield.$tracker.activeTracker;

                        if (item) {
                            port.postMessage({
                                message: "trackerresponse",
                                tracker: item,
                                options: myshield.$settings.options,
                                rank: myshield.$alexa.rankers[item.url],
                                usrank: myshield.$alexa.us_rankers[item.url],
                                online: (item.active === 1)
                            });
                        }
                    }
                    break;
                case 'blocksite':
                    {
                        myshield.$tracker.updateTrackSite(request.url, request.block, function(item) {
                            chrome.tabs.reload(request.tab);
                            port.postMessage({
                                message: "trackerresponse",
                                tracker: item,
                                options: myshield.$settings.options,
                                rank: myshield.$alexa.rankers[item.url],
                                online: true
                            });
                        });
                    }
                    break;
                case 'blockall':
                    {
                        console.log("request.block:" + request.block);
                        myshield.$settings.options.blockall = request.block;
                        myshield.$settings.options.tracking.$ad = request.block;
                        myshield.$settings.options.tracking.$social = request.block;
                        myshield.$settings.options.tracking.$analytics = request.block;
                        myshield.$settings.options.tracking.$sponsors = request.block;

                        myshield.$settings.setOptions(myshield.$settings.options);
                        if (myshield.$tracker.activeTracker && myshield.$tracker.activeTracker.blockall)
                            myshield.$tracker.activeTracker.blockall = request.block;
                        chrome.tabs.reload(request.tab);
                        if (!item)
                            item = myshield.$tracker.activeTracker;

                        port.postMessage({
                            message: "trackerresponse",
                            tracker: item,
                            options: myshield.$settings.options,
                            rank: myshield.$alexa.rankers[item.url],
                            usrank: myshield.$alexa.us_rankers[item.url],
                            online: true
                        });
                    }
                    break;
                case 'blockfeature':
                    {
                        //console.log("request.cat:" + request.cat + "|request.sub:" + request.sub + "|request.block:" + request.block);
                        myshield.$tracker.updateTrack(request.cat, request.sub, request.block, request.url, function(item) {
                            chrome.tabs.reload(request.tab);
                            port.postMessage({
                                message: "trackerresponse",
                                tracker: item,
                                options: myshield.$settings.options,
                                rank: myshield.$alexa.rankers[item.url],
                                usrank: myshield.$alexa.us_rankers[item.url],
                                online: true
                            });

                        });
                    }
                    break;
            }
        });
    },
    onMessage: function(request, sender, callback) {
        //console.log("Background script, onMessage:" + JSON.stringify(request));
        if (!request.tab) {
            request.tab = myshield.$tracker.activeTabId;
        }
        switch (request.message) {
            case 'trackpage':
                {
                    myshield.$analytics.trackPage(request.title, request.page);
                }
                break;
            case 'trackevent':
                {
                    myshield.$analytics.trackEvent(request.action, request.action, request.label, request.value);
                }
                break;
            case 'getoptions':
                {
                    chrome.tabs.sendMessage(request.tab, { message: "optionsresponse", options: myshield.$settings.options });
                }
                break;
            case 'savesettings':
                {
                    changed = false;
                    console.log("savesettings:" + JSON.stringify(request));
                    if (request.options.antiphishing !== myshield.$settings.options.antiphishing) {
                        console.log("antiphishing options changed to " + request.options.antiphishing);
                        changed = true;
                    }
                    myshield.$settings.options = request.options;
                    if (changed) {
                        myshield.$parser.getFeed();
                    }
                    chrome.storage.local.set({ "options": myshield.$settings.options }, function() {});
                }
                break;
        }
    },
    onMessageExternal: function(request, sender, callback) {
        if (request.message === 'version') {
            callback("__version__");
        }
    },
    onSelectionChanged: function(tabId, changeInfo) {
        if (myshield.$tracker.deferTab === tabId) {
            return;
        }
        //console.log("onSelectionChanged.tabId:" + tabId + "|" + JSON.stringify(changeInfo));

        myshield.$tracker.activeTabId = tabId;
        chrome.tabs.get(tabId, function(tab) {
            //console.log("onSelectionChanged.tabId:" + tabId + "|" + JSON.stringify(tab));
            myshield.$tracker.activeTabUrl = myshield.$api.cleanUrl(tab.url);
            if (!tab || !myshield.$api.checkUrl(myshield.$tracker.activeTabUrl)) {
                myshield.$tracker.updateButton(0);
                chrome.browserAction.setPopup({ popup: "" });
                return;
            } else {
                chrome.browserAction.setPopup({ popup: "html/popup.html" });
                myshield.$tracker.activeTabId = tab.id;
                if (myshield.$tracker.isBlockingPage(myshield.$tracker.activeTabUrl)) {
                    myshield.$tracker.updateButton(2);
                } else {
                    myshield.$tracker.updateButton(1);
                }
                myshield.$tracker.urlInfoChange(tab);
            }
        });
    },
    onTabUpdated: function(tabId, changeInfo, tab) {
        //console.log("onTabUpdated.tabId:" + tabId + "|" + JSON.stringify(changeInfo));
        if (tabId !== myshield.$tracker.activeTabId &&
            myshield.$tracker.activeTabId !== 0 &&
            myshield.$tracker.activeTabUrl == myshield.$api.cleanUrl(tab.url))
            return;
        //console.log("onTabUpdated.url:" + tab.url);

        myshield.$tracker.activeTabId = tab.id;
        myshield.$tracker.activeTabUrl = myshield.$api.cleanUrl(tab.url);
        //console.log("onTabUpdated.activeTabUrl:" + myshield.$tracker.activeTabUrl);
        if (!tab || !myshield.$api.checkUrl(tab.url)) {
            myshield.$tracker.updateButton(0);
            chrome.browserAction.setPopup({ popup: "" });
            return;
        } else {
            if (myshield.$tracker.isBlockingPage(tab.url)) {
                myshield.$tracker.updateButton(2);
            }

            chrome.browserAction.setPopup({ popup: "html/popup.html" });
        }
        //console.log("onTabUpdated." + tabId + "|changeInfo:" + JSON.stringify(changeInfo) + "|tab:" + JSON.stringify(tab));
        var url = myshield.$api.cleanUrl(tab.url);
        var checkItem = myshield.$api.getTrack(url);
        if (checkItem) {
            checkItem = myshield.$tracker.activeTracker;
            checkItem.previousresults = 0;
            //console.log("onTabUpdated.assign to activeTracker" + tabId + "|tab.url=" + url);
        }
        myshield.$tracker.urlInfoChange(tab);
    },
    onTabCreated: function(tab) {
        //try to kill popup here
        chrome.windows.get(tab.windowId, function(chromeWindow) {
            if (chromeWindow && (chromeWindow.state == "fullscreen" || chromeWindow.type === "popup") && myshield.$settings.options.popup === 1) {
                //myshield.$tracker.activeTabId = tab.openerTabId;
                myshield.$tracker.deferTab = tab.id;
                chrome.windows.remove(chromeWindow.id, function() {
                    myshield.$tracker.activeTabId = myshield.$tracker.openerTab;
                    //chrome.tabs.update(myshield.$tracker.openerTab, {selected: true});
                    myshield.$tracker.block = 1;
                    myshield.$tracker.deferTab = -1;
                    myshield.$tracker.updateButton(1);
                    //console.log("close popup");
                });
                return;
            } else {
                myshield.$tracker.deferTab = -1;
            }
        });
    },
    onCommitted: function(tab) {
        var isMainFrame = (tab.frameId === 0);
        if (tab.tabId !== myshield.$tracker.activeTabId) //|| !isMainFrame)
            return;

        if (isMainFrame) {
            if (myshield.$tracker.isBlockingPage(tab.url)) {
                myshield.$tracker.updateButton(2);
            } else {
                myshield.$tracker.updateButton(1);
            }
        }
        // console.log("onCommitted:" + JSON.stringify(obj) + "|" + isMainFrame);
        var item = myshield.$api.getTrack(myshield.$api.cleanUrl(tab.url));
        if (!item)
            item = myshield.$tracker.activeTracker;
        if (item) {
            chrome.tabs.sendMessage(tab.tabId, {
                message: "trackerresponse",
                tracker: item,
                options: myshield.$settings.options,
                rank: myshield.$alexa.rankers[item.url],
                usrank: myshield.$alexa.us_rankers[item.url],
                online: true
            });
        }
    },
    $init: function() {
        console.log("init");
        var now = parseInt(new Date().getTime() / 1000);
        chrome.storage.local.get("beatLastUpdate", function(result) {
            myshield.$api.beatLastUpdate = (typeof result.beatLastUpdate !== "undefined") ? result.beatLastUpdate : 0;
            beatUpdateDue = parseInt(myshield.$api.beatLastUpdate) + parseInt(myshield.$api.beatInterval / 1000);
            if (beatUpdateDue > now) {
                console.log("Too soon to send heart beat!");
                return;
            }
            setTimeout(function() {
                myshield.$analytics.trackEvent("Heartbeat_DailyUser", " ", " ", " ");
            }, 1000);
            chrome.storage.local.set({ "beatLastUpdate": now }, function() {
                myshield.$api.beatLastUpdate = now;
                console.log("beatLastUpdate sent at " + new Date());
            });
        });
        chrome.storage.local.get("trackerLastUpdate", function(result) {
            myshield.$api.trackerLastUpdate = (typeof result.trackerLastUpdate !== "undefined") ? result.trackerLastUpdate : 0;
            trackerUpdateDue = parseInt(myshield.$api.trackerLastUpdate) + parseInt(myshield.$api.updateInterval / 1000);
            if (trackerUpdateDue > now) {
                console.log("Too soon to update tracking list " + parseInt(trackerUpdateDue - now) + " secs left. Keeping old list");
                return;
            }
            myshield.$tracker.getRemoteInfo(myshield.$api.baseurl, function(tracker_str) {
                try {
                    var tacker_json = JSON.parse(tracker_str);
                    tacker_json = myshield.$tracker.initTrack(tacker_json);
                    tracker_str = null;
                    chrome.storage.local.set({ "tracker": JSON.stringify(tacker_json) }, function() {
                        myshield.$api.tracker = tacker_json;
                        console.log("tracker saved at " + new Date());
                    });

                    chrome.storage.local.set({ "trackerLastUpdate": now }, function() {
                        myshield.$api.trackerLastUpdate = now;
                        console.log("trackerLastUpdate saved at " + new Date());
                    });
                } catch (e) {
                    console.log("something went wrong here...not updating trackers.error:" + e.message);
                }
            }, "arraybuffer");
        });
    }
};

document.addEventListener("DOMContentLoaded", function() {
    myshield.$tracker.$init();
    if (myshield.$tracker.debug) {
        console.log("myshield.$api.updateInterval=" + myshield.$api.updateInterval);
    }
    setInterval(function() { myshield.$tracker.$init(); }, myshield.$api.updateInterval);
    chrome.webRequest.onBeforeRequest.addListener(myshield.$tracker.onBeforeRequest, { urls: ["<all_urls>"] }, ["blocking"]);
    chrome.webNavigation.onBeforeNavigate.addListener(myshield.$tracker.onBeforeNavigate);
    chrome.extension.onMessage.addListener(myshield.$tracker.onMessage);
    chrome.runtime.onMessageExternal.addListener(myshield.$tracker.onMessageExternal);
    chrome.runtime.onConnect.addListener(myshield.$tracker.onPopupMessage);

    // Fired when a tab is switched.
    chrome.tabs.onSelectionChanged.addListener(myshield.$tracker.onSelectionChanged);
    chrome.tabs.onCreated.addListener(myshield.$tracker.onTabCreated);
    chrome.tabs.onUpdated.addListener(myshield.$tracker.onTabUpdated);
    chrome.webNavigation.onCommitted.addListener(myshield.$tracker.onCommitted);
});