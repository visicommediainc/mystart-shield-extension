myshield.$events = {
    UNSAFE_URL: "http://mystartshield.com/extensions/blocked.html?url=",
    SAFE_URL  : "http://mystartshield.com/extensions/safe.html",
    INSTALLED_URL :"html/install.html",
    UNINSTALL_URL:"http://mystartshield.com/extensions/uninstall.html?cid=",
    isBlockingPage : function(url) {
        if (!myshield.$settings.options.antiphishing) return;
        var stripurl = url.replace("https://", "").replace("http://", "");
        var domain = myshield.$events.getDomain(url);
        var isWhite = (myshield.$parser.white.indexOf(domain) !== -1);
        //console.log("10.domain:" + domain + "|isWhite:" + isWhite);
        var issafe = !myshield.$parser.mwlist || typeof myshield.$parser.mwlist[stripurl] == "undefined"|| isWhite ;
        var UNSAFE_URI_RE = /(?=mystartshield.com)(?=.*blocked.html)/gi;
        if (url && url.match(UNSAFE_URI_RE) || !issafe) {
            return true;
        } 
        return false;
    },
    getDomain: function(url)
    {
        if (url === null)
            return url;
        var match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
       if (match !== null && match.length > 2 && typeof match[2] === 'string' && match[2].length > 0) {
           url = match[2];
       }
        subs = url.split(".");
        if (subs.length > 2)
        {
            return subs[1] + "." + subs[2];
        }
        else
            return url;
    },
    onInstalled : function(details) {
        if (details.reason == "update") {
            myshield.$analytics.trackEvent("Runtime_Update", " ", " ", " ");
        }
    },
    onClicked : function(tab) {
        if(!tab || !myshield.$api.checkUrl(tab.url))
        {
            myshield.$tracker.updateButton(0);
		    chrome.browserAction.setPopup({popup: ""});
            return;
	    }else{
		    chrome.browserAction.setPopup({popup: "html/popup.html"});
		}
        myshield.$analytics.trackEvent("Button_Click", " ", tab.url, " ");
    },
    onBeforeNavigate : function(details) {
        if (!myshield.$settings.options.antiphishing)
            return;
        var url = details.url;
        var stripurl = details.url.replace("https://", "").replace("http://", "");
        var domain = myshield.$events.getDomain(url);
        var isWhite = (myshield.$parser.white.indexOf(domain) !== -1);
        //console.log("58.domain:" + domain + "|isWhite:" + isWhite);
        var issafe = !myshield.$parser.mwlist || typeof myshield.$parser.mwlist[stripurl] == "undefined"|| isWhite ;
        if (!issafe && details.url.indexOf("alwd=1") === -1)
        {
            chrome.tabs.update(details.tabId, {
                url: myshield.$events.UNSAFE_URL + details.url
            });
        }
    },
    onBeforeRequest : function(details) {
        var url = details.url;
        if(url.indexOf("_chromeextension_unknown__") != -1 && url.indexOf("__campaign__") != -1 && url.indexOf("mystartshield.com") != -1){
            url = url.replace("_chromeextension_unknown__", "");
            url = url.replace("__campaign__", myshield.$broker.campaign_id);
            params = encodeURIComponent(url.split("?")[1].split('&q=')[0]);
            console.log("searchParams:" + params);
            myshield.$analytics.trackEvent("Action_Search", "Omnibar_Search", myshield.$broker.campaign_id , params);
            return { redirectUrl: url };
        }
    },
    onCommitted : function(obj) {
        if (obj.tabId !== myshield.$tracker.activeTabId)
            return;
        unsafe = myshield.$events.isBlockingPage(obj.url);
        status = unsafe ? 2: 1;
        myshield.$tracker.updateButton(status);
     },
    onMessage : function(request, sender, sendResponse) {
	    switch (request.directive) {
            case "popup-click":
                myshield.$analytics.trackEvent("Button_Click", " ", " ", " ");
            break;
            default:
	    }
	   
	    if (!request.tab)
	    {
	        request.tab = myshield.$tracker.activeTabId;
	    }
        if (request.from == "scanlink") {
            var stripurl = request.url.replace("https://", "").replace("http://", "");
            //console.log("stripurl:" + stripurl);
            /*
            myshield.$db.getResult(stripurl, function (stripurl, result) {
                console.log("url:" + stripurl + "|" + JSON.stringify(result));
                issafe = (typeof result  == "undefined");
                chrome.tabs.sendMessage(request.tab , {message: "scanresponse",
                    linkIndex: request.linkIndex,
                    anchor : request.anchor,
                    isSafe: issafe

                });
            });
            */
            var issafe = !myshield.$parser.mwlist || typeof myshield.$parser.mwlist[stripurl] == "undefined";
            chrome.tabs.sendMessage(request.tab , {message: "scanresponse",
                linkIndex: request.linkIndex,
                anchor : request.anchor,
                isSafe: issafe
           });
        }
    }
};

chrome.runtime.onInstalled.addListener(myshield.$events.onInstalled);

chrome.browserAction.onClicked.addListener(myshield.$events.onClicked);

chrome.webNavigation.onBeforeNavigate.addListener(myshield.$events.onBeforeNavigate);

chrome.webRequest.onBeforeRequest.addListener(myshield.$events.onBeforeRequest ,{urls: ["<all_urls>"]},["blocking"]);

chrome.webNavigation.onCommitted.addListener(myshield.$events.onCommitted);

chrome.runtime.onMessage.addListener(myshield.$events.onMessage);
