if (!myshield)
    var myshield = {};

myshield.$analytics = {
    $ga : {
        _AnalyticsCode: "UA-87400040-2",
        init: function(){
        },
        trackPage : function(page, customUrl) {
            var request = new XMLHttpRequest();
            var message =
              "v=1&tid=" +  myshield.$analytics.$ga._AnalyticsCode + "&cid=" + myshield.$broker.user_id + "&aip=1" +  
              "&cn=" +  myshield.$events.campaign_id + "&ds=add-on&t=pageview&dp=" + page + "&dr=" + customUrl ;
             
            console.log("trackPage.message=" + message);
            request.open("POST", "https://www.google-analytics.com/collect", true);
            request.send(message);
        },
        trackEvent: function(category, action, label, value) {
            var request = new XMLHttpRequest();
            var message  = "tid=" +  myshield.$analytics.$ga._AnalyticsCode + "&cid=" + myshield.$broker.user_id ;
                message += "&cn=" +  myshield.$broker.campaign_id + "&t=event&ec=" + category + "&v=1";
  
            if (action && action !== '')
            message += "&ea=" + action ;
            
            if (label && label !== '')
            message += "&el=" + label;

            console.log("ga.trackEvent.message=" + message);
            request.open("POST", "https://www.google-analytics.com/collect", true);
            request.send(message);
        }
    },
    $piwik : {
        url : "https://analytics.vmn.net/",
        cid : 8,
        pwTracker : {},
        getPwTracker : function (baseUrl, siteId){
	        var u = baseUrl.replace(/\/$/, "");
	        var tracker = function(){
		        // arguments does not implement Array common function (shift, slice....)
		        // Piwik make use of these function on _paq instance thus the necessity 
		        // of casting arguments as Array
                (window._paq = window._paq || []).push(Array.prototype.slice.call(arguments));
	        };
            tracker = Piwik.getTracker(u + '/piwik.php', siteId);
	        return tracker;
        },
        init : function() {
    	    myshield.$analytics.$piwik.pwTracker = myshield.$analytics.$piwik.getPwTracker(myshield.$analytics.$piwik.url ,myshield.$analytics.$piwik.cid);
        },
        trackPage : function(page, customUrl) {
            var url = 'http://mystartshield.com/?pk_campaign=' + myshield.$broker.campaign_id;
            myshield.$analytics.$piwik.pwTracker.setUserId(myshield.$broker.user_id);
 	        myshield.$analytics.$piwik.pwTracker.setCustomUrl(url);
	        myshield.$analytics.$piwik.pwTracker.setDocumentTitle(page);  // replace wlcome with the page name
	        myshield.$analytics.$piwik.pwTracker.trackPageView(); // replace welcome with page name
        },
        trackEvent : function(category, action, label, value) {
            var url = 'http://mystartshield.com/?pk_campaign=' + myshield.$broker.campaign_id;
            console.log("piwik.trackEvent.url=" + url);
            myshield.$analytics.$piwik.pwTracker.setUserId(myshield.$broker.user_id);
	        myshield.$analytics.$piwik.pwTracker.setCustomUrl(url);
	        myshield.$analytics.$piwik.pwTracker.trackEvent(category, action, label, value);
        }
    },
    trackPage : function(page, customUrl) {
        myshield.$analytics.$ga.trackPage(page, customUrl);
        myshield.$analytics.$piwik.trackPage(page, customUrl);
    },
    trackEvent : function(category, action, label, value) {
        myshield.$analytics.$ga.trackEvent(category, action, label, value);
        myshield.$analytics.$piwik.trackEvent(category, action, label, value);
        console.log("trackEvent:" + category + "|" + action + "|" + label + "|" + value);
    }
};

document.addEventListener("DOMContentLoaded", function() {
    myshield.$analytics.$ga.init();
    myshield.$analytics.$piwik.init();
});
