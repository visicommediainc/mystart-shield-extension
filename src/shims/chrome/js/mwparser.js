myshield.$parser = {
    mwlist : {},
    timestamp :0 , mwtimestamp : 0, mwfulllastdownload : 0,
	mwdeltalastdownload : 0,
    currentDateMs : new Date().getTime(),
    charDecrypt : [],
    white : [],
    UpdateInterval : 25 * 60 * 1000,
    updateFullInterval : 7 * 24 * 60 * 60 * 1000,
    api : {
        timestamp : 0 ,
        baseurl: "http://urlfilter2.vmn.net/panda/panda/",
        data: "data/",
        timelink: "stamp.txt",
        whitelink: "white.list",
        cataloglist: "catalog.list"
    },
    initCharValues : function() {
        for($i = 0; $i < 256; ++$i)
        {
            byte = ($i & 0x1F) | ($i & 0xC0) >> 1 | ($i & 0x20) << 2;
            this.charDecrypt[$i] = byte;
        }
    },
    adjustTimeFormat : function(d) {
        var dx = parseInt(d);
        if (dx < 10)
        dx = "0" + dx;
        return dx;
    },
    getTimeStamp : function() {
        var d = new Date();
        var yy = String(d.getFullYear()).match(/\d{2}$/);
        var mm = this.adjustTimeFormat(parseInt(d.getMonth() + 1));
        var dd = this.adjustTimeFormat(d.getDate());
        var hh = this.adjustTimeFormat(d.getHours());
        var MM = this.adjustTimeFormat(d.getMinutes());
        var ss = this.adjustTimeFormat(d.getUTCSeconds());
        return yy + "" + mm + "" + dd + "" + hh + "" + MM + "" + ss;
    },
    isFullUpdate : function(lastfullupdate) {
        var yy = String(new Date().getFullYear()).substr(0,2) + lastfullupdate.substring(0,2);
        var mm = lastfullupdate.substring(2,4);
        var dd = lastfullupdate.substring(4,6);
        var hh = lastfullupdate.substring(6,8);
        var MM = lastfullupdate.substring(8,10);
        var ss = lastfullupdate.substring(10,12);
        var d1 = new Date(yy + "-" + mm + "-" + dd + " " + hh + ":" + MM + ":" + ss).getTime();
        if ((parseInt(d1) + myshield.$parser.updateFullInterval) <= this.currentDateMs)
            return true;
        return false;
    },
    getRemoteInfo : function(url, callback, type) {
        var xhr = new XMLHttpRequest();
        xhr.onload = function() {
            if (type === "arraybuffer")
            {
                var arrayBuffer = xhr.response;
                if (arrayBuffer) {
                    var byteArray = new Uint8Array(arrayBuffer);
                    callback(byteArray);
                    arrayBuffer = null;
                    byteArray = null;
                    xhr = null;
                }
            }else{
                callback(xhr.responseText);
                xhr = null;
            }
        };
        if (type === "arraybuffer")
          xhr.responseType = "arraybuffer";

        xhr.open("GET", url, true);
        xhr.send();
    },
    parseMwList : function(data, key, type, filestamp, callback) {
        if(data === null)
            return;
        var now = 0 ;//Date.now();
        var list = {};
        var url = '';
        var count = 0;
        for (var idx=0 ; idx <data.length;idx++)
        {
            //if (count === 10)
            //    break;
            var c = String.fromCharCode(myshield.$parser.charDecrypt[data[idx]]);
            if(c === '\n'){
                 if (myshield.$parser.white.indexOf(url.substring(0, url.length-1)) != -1 || url.length === 0)
                 {
                    continue;
                 }
                 list[url] = now ;
                 count ++ ;
                url = '';
            }else{
                url += c;
           }
        }
        if (type === '-f')
        {
            mwlist = list;
        }
        else if (type === '-l')
        {
            //console.log("less:" + JSON.stringify(list));
            new_mwlist = {};
            for(var entry in myshield.$parser.mwlist) {
                if (myshield.$parser.white.indexOf(entry.substring(0, entry.length-1)) != -1)
                {
                    console.log("white list url=" + url + "\n");
                    continue;
                }
                if (typeof(list[entry]) != "undefined")
                    continue;
                new_mwlist[entry] = myshield.$parser.mwlist[entry];
            }
            mwlist = new_mwlist ;
        }
        else if (type == "-m")
        {
            //console.log("more:" + JSON.stringify(list));
            mwlist = myshield.$parser.mwlist;
            for(var more in list) {
                mwlist[more] = list[more];
            }
        }

        for (var cat in myshield.$api.tracker)
        {
           if (cat === "$spyware")
           {
                for (var it in myshield.$api.tracker[cat])
                {
                    mwlist[myshield.$api.tracker[cat][it].url] = 0;
                }
           }
        }
        myshield.$parser.mwlist = mwlist;
        url = null;
        list = null;
        mwlist = null;
        data = null;
        if (typeof new_mwlist !== 'undefined')
            new_mwlist = null;
        if (callback)
            callback();
        return;
    },
    downloadFeed : function(type, filestamp) {
         if (!type)
            return;
        console.log("downloadFeed.type=" + type + "|filestamp=" + filestamp);
        switch(type)
        {
            case "-f":
            {
                myshield.$parser.getRemoteInfo(myshield.$parser.api.baseurl  +myshield.$parser.api.data + filestamp + type + ".br", function(byteArray) {
                     myshield.$parser.parseMwList(byteArray, "mwlist", type, filestamp, function() {
                        byteArray = null;
                        chrome.storage.local.set({"mwfulllastdownload": filestamp});
                        chrome.storage.local.set({"mwtimestamp": myshield.$parser.timestamp});
                        myshield.$db.setData("mwlist", myshield.$parser.mwlist);
                        console.log("full done.timestamp:" +  myshield.$parser.timestamp + "|filestamp:" + filestamp);
                    });
                }, "arraybuffer");
            }
            break;
            case "-d":
            {
                xtype = "-l";
                myshield.$parser.getRemoteInfo(myshield.$parser.api.baseurl  +myshield.$parser.api.data + filestamp + xtype + ".br", function(byteArray) {
                    myshield.$parser.parseMwList(byteArray, "mwlesslist", xtype, filestamp, function() {
                        //console.log("less done");
                        xtype = "-m";
                        myshield.$parser.getRemoteInfo(myshield.$parser.api.baseurl  +myshield.$parser.api.data + filestamp + xtype + ".br", function(byteArray) {
                            myshield.$parser.parseMwList(byteArray, "mwmorelist", xtype, filestamp, function() {
                                myshield.$db.setData("mwlist", myshield.$parser.mwlist);
                                console.log(filestamp + " delta done");
                            });
                        });

                    }, "arraybuffer");
                }, "arraybuffer");
            }
            break;
        }
    },
    getWhichDownload : function(error) {
        if(!myshield.$parser.mwlist || !myshield.$parser.mwtimestamp || !myshield.$parser.mwfulllastdownload || error) {
            myshield.$parser.downloadFeed("-f", myshield.$parser.api.timestamp);
            return ;
        }
        else if (myshield.$parser.mwtimestamp)
        {
            var isReadyForFullUpdate = myshield.$parser.isFullUpdate(myshield.$parser.mwtimestamp);
            //console.log("getWhichDownload.isReadyForFullUpdate=" + isReadyForFullUpdate);
            if (isReadyForFullUpdate) {
                myshield.$parser.downloadFeed("-f", myshield.$parser.api.timestamp);
                return ;
            }
            else
            {
               // console.log("getWhichDownload.check for delta updates");
                if (myshield.$parser.api.timestamp > myshield.$parser.mwfulllastdownload){
                    if (!myshield.$parser.mwdeltalastdownload || (myshield.$parser.mwdeltalastdownload && myshield.$parser.api.timestamp > myshield.$parser.mwdeltalastdownload)) {
                        //console.log("getWhichDownload.starting delta updates..");
                        myshield.$parser.getRemoteInfo(myshield.$parser.api.baseurl + myshield.$parser.api.cataloglist, function(reply){
                            Array.max = function(array) {
                                return Math.max.apply(Math, array);
                            };
                            var timestamps = reply.split('\n');
                            max= Array.max(timestamps);
                            reply = null;
                            function setDelayDownload(value) {
                                setTimeout( function() {
                                    myshield.$parser.downloadFeed("-d", value);
                                } , 1000);
                            }

                            if (max > myshield.$parser.mwfulllastdownload || (myshield.$parser.mwdeltalastdownload && max >= myshield.$parser.mwdeltalastdownload))
                            {
                                var count = 0;
                                for (var i=0; i<timestamps.length;i++){
                                    var ivalue = timestamps[i];
                                    if (ivalue <= myshield.$parser.mwfulllastdownload || (myshield.$parser.mwdeltalastdownload && ivalue <= myshield.$parser.mwdeltalastdownload))
                                        continue;
                                    count++;
                                }

                                if (count > 6){
                                    console.log("getWhichDownload.bailout and download full update");
                                    myshield.$parser.downloadFeed("-f", myshield.$parser.api.timestamp);
                                    timestamps = null;
                                    return ;
                                }

                                for (var j=0; j<timestamps.length;j++){
                                    var value = timestamps[j];
                                    if (value <= myshield.$parser.mwfulllastdownload || (myshield.$parser.mwdeltalastdownload && value <= myshield.$parser.mwdeltalastdownload))
                                        continue;

                                    console.log("getWhichDownload.deltaupdates.timevalue=" + value);
                                    setDelayDownload(value);
                                }
                                chrome.storage.local.set({"mwdeltalastdownload": max});
                                timestamps = null;
                            }
                        });
                    }
                }
            }
        }
    },
    getFeed : function() {
        if (!myshield.$settings.options.antiphishing)
        {
            myshield.$parser.mwlist = {};
            return;
        }
        myshield.$api.getValue("mwfulllastdownload", function (result1) {
            myshield.$parser.mwfulllastdownload = result1.mwfulllastdownload;
            myshield.$api.getValue("mwtimestamp", function (result2) {
                myshield.$parser.mwtimestamp = result2.mwtimestamp;
                myshield.$api.getValue("mwdeltalastdownload", function (result3) {
                    myshield.$parser.mwdeltalastdownload = result3.mwdeltalastdownload;
                    myshield.$db.getData("mwlist", function (result4) {
                        var error = false;
                        try{
                            myshield.$parser.mwlist = result4; //JSON.parse(result4.mwlist);
                        }catch(ex){ myshield.$parser.mwlist = {};error = true;}
                        if(myshield.$parser.mwlist === null)
                        {
                            error = true;
                        }
                        myshield.$parser.getRemoteInfo(myshield.$parser.api.baseurl + myshield.$parser.api.whitelink, function(reply){
                            myshield.$parser.white = reply.split('\n');
                            myshield.$parser.getRemoteInfo(myshield.$parser.api.baseurl + myshield.$parser.api.timelink, function(reply){
                                myshield.$parser.api.timestamp = reply;
                                myshield.$parser.timestamp = myshield.$parser.getTimeStamp();
                                myshield.$parser.getWhichDownload(error);
                            });
                        });
                        return;
                    });
                });
            });
       });
    }
};

myshield.$parser.initCharValues();
document.addEventListener("DOMContentLoaded", function() {
    setInterval(function() { myshield.$parser.getFeed();}, myshield.$parser.UpdateInterval);
}, false);