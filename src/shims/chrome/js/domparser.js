if (!myshield)
    var myshield = {};

function on(elms, eventName, eventHandler)
{
    for(i=0; i<elms.length;i++){
        el = elms[i];
        el.addEventListener(eventName, eventHandler);
    }
}

function $(tag, parent)
{
    if (parent === null || typeof parent === 'undefined')
    {
        parent = document;
    }else {
        parent = parent.ownerDocument;
    }
    if (tag.includes("#"))
    {
        return parent.getElementById(tag.replace("#", ""));
    }
    if (tag.includes("."))  
    {  
        return parent.getElementsByClassName(tag.replace(".", ""));
    }
}

myshield.$domParser = {
    firstImg : null ,
    init : function()
    {
        //console.log("domparser.init");
        chrome.extension.onMessage.removeListener(myshield.$domParser.onMessageFromBG);
        chrome.extension.onMessage.addListener(myshield.$domParser.onMessageFromBG);
		window.parent.postMessage({message: "getoptions"}, '*');
    },
    cleanup : function() {
        myshield.$domParser.generateIcon = null;
        myshield.$domParser.popup = {};
    },
    onMessageFromBG : function(request, sender, sendResponse){
 	    if (!request.message ) return;
 	    var currentUrl = document.location.hostname + "";
        if (currentUrl.indexOf("yahoo.") == -1 &&
            currentUrl.indexOf("bing.com") == -1 &&
            currentUrl.indexOf("google.") == -1 )
            {
                myshield.$domParser.cleanup();
                return;
            }

        search = "";
        if (currentUrl.indexOf("yahoo.") != -1)
            search = "yahoo";
        else if (currentUrl.indexOf("bing.com") != -1)
            search = "bing";
        else if (currentUrl.indexOf("google.") != -1)
            search = "google";

        if (search === "")
        {
            myshield.$domParser.cleanup();
            return;
	    }
	    switch(request.message){
            case 'optionsresponse':
            {
                //console.log("domParser.optionsresponse:" + JSON.stringify(request));
                var options = request.options;
                if ( (options.search.bing === 1 && currentUrl.indexOf("bing.") !== -1) ||
                     (options.search.yahoo === 1 && currentUrl.indexOf("yahoo.") !== -1)||
                     (options.search.google === 1 && currentUrl.indexOf("google.") !== -1 ))
                    {
                        myshield.$domParser.inject(options, search);
                    }
                    else
                    {
                        myshield.$domParser.cleanup();
                    }
            }
            break;
            case 'scanresponse':
            {
                //console.log("domParser.scanresponse:" + JSON.stringify(request));
                myshield.$domParser.responseFunc(request);
            }
            break;
           case 'removeallscripts':
            {
                //console.log("domParser.scanresponse:" + JSON.stringify(request));
                myshield.$domParser.removeAllScripts();
            }
            break;
         }
    },
    removeAllScripts: function(){
	    var r = document.getElementsByTagName('script');
        for (var i = (r.length-1); i >= 0; i--) {

            if(r[i].getAttribute('id') != 'a'){
                r[i].parentNode.removeChild(r[i]);
            }
        }
    },
    inject : function(options, search){
        //console.log("inject");
        myshield.$domParser.popup.init();
        switch(search)
        {
            case "yahoo":
            {
                links = document.getElementsByTagName("a");
                for (var j = 0; j < links.length; j++) {
                    if (( links[j].className.indexOf("yschttl") !== -1 ||
                          links[j].className.indexOf("myshield_item") !== -1 ||
                          links[j].className.indexOf("td-u") !== -1) &&
                          links[j].href.indexOf("http") !== -1) {
                        if (typeof links[j].href !== "undefined") {
                            chrome.runtime.sendMessage({
                                from: "scanlink",
                                anchor: "a",
                                url: links[j].href,
                                linkIndex: j
                            });
                        }
                    }
                }
            }
            break;
            case "bing":
            {
                className = "b_algo";
                elms = document.getElementsByClassName(className);
                //console.log("className." + elms.length);
                for (var i = 0; i < elms.length; i++)
                {
                    anchor = elms[i].getElementsByTagName("a")[0];
                    if (anchor && anchor.href.indexOf("http") !== -1) {
                            if (anchor.href !== "undefined")
                            {
                                chrome.runtime.sendMessage({
                                    from: "scanlink",
                                    anchor: className,
                                    url: anchor.href,
                                    linkIndex: i
                                });
                            }
                    }
                }
            }
            break;
            case "google":
            {
                window.addEventListener("message", function(event) {
                    if (event.source != window) return;
                    var d = event.data;
                    if (typeof d !== "object") return;
                    //console.log("d=" + JSON.stringify(d));
                    className = "r";
                    elms = window.document.getElementsByClassName(className);
                    //console.log("className." + elms.length);
                    for (var i = 0; i < elms.length; i++)
                    {
                        anchor = elms[i].getElementsByTagName("a")[0];
                        if (anchor && anchor.href.indexOf("http") !== -1 &&
                            elms[i].innerHTML.indexOf("myshield") === -1) {
                                if (anchor.href !== "undefined")
                                {
                                    chrome.runtime.sendMessage({
                                        from: "scanlink",
                                        anchor: className,
                                        url: anchor.href,
                                        linkIndex: i
                                    });
                                }
                        }
                    }
                });
            }
            break;
        }
    },
    generateIcon : function(url, isSafe) {
        allow_ico = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAATCAYAAAByUDbMAAAACXBIWXMAAA7EAAAOxAGVKw4bAAACR0lEQVQ4y6WUT2gTQRTGk2gaSkXobSetFqEGqqASix487MwkKUG8yM5ueqgiIihUvFhEbG8KIljtxUMPUry0O8EgvYgFD+JNsNiLolR3NoGCqOCfYg/Wtn4rm2TTNk3EgQ8Sdt6P733v7YZCW5yUdMM8r/ZwWx3MTpe2hf71ZOxShEvVyqTqZbYap7azCP3C72lA9Z6zN6KdPBepC6C2SqA4R6VzFUUTKH4LrUJrvlagd0duzwwQXdzXdDGuUeMioeI4YaKnI5ULB2DOrUBhUB7wE516f7f79IjQdGMWWqsRFc8JM1vqwZahL9ArKtU1OO7u6jvVCjeXUTwHfYV+Nwsb5dJNZB+p7evj6Mr0t6DNA4AMQUubw6RzswKT6lyjIRFqHAbom+/uGbKrgQ2XYWjrSiMY3FBAfvqwx3EqopWHmOCZCsx27jWE6YYFLXswuJzYnTGrqwI3yUBmcycKbnRrmBgrDwAtDtU8ZLLYBkjRh/3A0h6rB+qgoh2Q1z5sBbDemgvpQjHCpHOnOgQnn364uTtM80K5RehlnIm2DZd43t3r75cHXELr59ffQdCHAHB90CpcDWgpM7zxnZQluFMjgew+47/ZN/Px72XCrATRjReB7X+K/dpRN1gU7wSkEADO88n5JFrLoviN58YHKY2a+xt+NbgsEkAn6dSH6/sujZ2EowcoXqw6EgtEN1mcWeGmPkNHR5/EABlE8feAG0+zyC0Zp02CKmFzK4aFtPyXewFhD6Pd9tD/nF3p/mhnKhcj3Gjo5g+K12i+TrDYfwAAAABJRU5ErkJggg==";
        block_ico = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABMAAAATCAYAAAByUDbMAAAAAXNSR0IArs4c6QAAAv1JREFUOBF9U11IFFEUnju7y27lTjvG2ujqNrL9EYiGYmEhkkI1ZM9SL4XZD2EZQQ89hC9BFPZiP7APEfQUPmUPkT0US4RWT9GamZiu64IphljO6jhz++5177rS6oWZ8/Od+91zzj2XSBusS+Gw6ljWWSpJQYmQx9FUKrFBuERyQUopuRCJKC7T3GlL0jHYJ4BX4/OA8Kssy99uhsN3vB7PhBaLzRBC4F5dnOy8pl2mhOyHUQYCFUE6ZHA1TJJkSfpV7ff3NwUCJTJjoXQGviGH0nG9vPwh6elZcmc2tEiUHhbHIDDLgwMsr9vd3xIM/i72eGoRp4GA48j+KHgTyWTyJRwjIOfLyUghKEgm0KfX1OVqPakoV0G0BUSWCBASZEtCF5kJm5XzEZvuU49n0OfzDXePjCxG29pkKR43konEXpxaKTnOaZZVdlNGEWTZuqCMRqemnucGks5OljnL4Av7ftbWbsaBnAwtcdBvvl+UOSc2A1CFvp5E94tysAXbsv4ym5MRWZ4WIHrl79R1n7DzSaSpCT8ONzep6jyzRWY/BAgZTi0v78qx16ip5mZWYqVwgmB2e12dyWxOhoLfIKMp5kAPStHg40zPt+xUqgK3vDuLEfIq09MVMreqDiPguwgA4an2UKhU2ELShga3I8vXYPOBxliMu2Q5JnCe2aN4/I+LkGcA0xxAGWnb7urQ9YAIZHJsYeGBQ4ghfLiI96UVFUPCdgnlgKKMY7JrYEe4j5CIZdvhmqKit5+bmuiZgoIr8Lci620MR1tG8VavB3p7kzwevyzZp/l586CiDNqUNsJfiM+NTPegnuKIbR9BXzvQ0BUiQuaoLN8uHxh4IYiYxAFr1zlNM1B7l+b1zjZu3Tod8nojmOx9GAfeEhAuYpSe6lVV7SQaXfO8/iNj1BdLSg7dCIWeQF29NRjIdBKiWzeMe+IGYWdXXjKGTtbXly2a5i2kY6Dpfrg+QL+7wzDe5SNie9YlYyAbhbF0uhpBhT5FiWl9ffzZMCzf+gfeWA1e/0h9uQAAAABJRU5ErkJggg==";
        var shield = {
            red: "http://mystartshield.com/extensions/blocked.html?url=",
            green: "http://mystartshield.com/extensions/safe.html"
        };
        var iconCSS = {
            "margin-left": "10px",
            display: "inline-block",
            width: "20px",
            height: "18px",
            "background-image": 'url("' + shield.allow_ico + '")',
            "background-repeat" : 'no-repeat',
            "vertical-align": "middle"
        };
        function css(style) {
            var out = "";
            var addStyle = function(style) {
                for (var property in style) {
                    out += property + ":" + style[property] + ";";
                }
            };
            if (style.length) {
                for (var i = 0; i < style.length; i++) {
                    addStyle(style[i]);
                }
            } else {
                addStyle(style);
            }
            return out;
        }
        if (isSafe) {
            return "<div class='myshield_item safe' title='SECURE' onclick='event.preventDefault();document.location.href=\"" + shield.green + "\"' style='" + css([ iconCSS, {
                "background-image": 'url("' + allow_ico + '")'
            } ]) + "'></div>";
        } else {
            var redurl = shield.red + url;
            return "<div class='myshield_item unsafe' title='NOT SAFE' onclick='event.preventDefault();document.location.href=\"" + redurl + "\"' style='" + css([ iconCSS, {
                "background-image": 'url("' + block_ico + '")'
            } ]) + "'></div>";
        }
    },
    responseFunc: function(response) {
        if (!response) return;
        //console.log("response:" + JSON.stringify(response));
        if (response.anchor === "a")
        {
            anchor = document.getElementsByTagName(response.anchor);
            link = links[response.linkIndex];
        }
        else
        {
            anchor = document.getElementsByClassName(response.anchor);
            link =  anchor[response.linkIndex].getElementsByTagName("a")[0];
        }

        if (link.innerHTML.indexOf("myshield") !== -1)
            return;
        //console.log("responseFunc.anchor:" + anchor.length);
        isSafe = response.isSafe;
        if (link && link.parentNode && link.parentNode.nextSibling && link.parentNode.nextSibling.outerHTML &&
                    link.parentNode.nextSibling.outerHTML.indexOf("Warning: Dangerous") != -1)
            isSafe = false;
        link.innerHTML += myshield.$domParser.generateIcon(link.href, isSafe);
        on($(".myshield_item"), "mouseover", function(e) {
            //console.log("mouseover");
            clearTimeout(myshield.$domParser.popup.hideTimer);
            myshield.$domParser.popup.showTimer = setTimeout(function(){ myshield.$domParser.popup.show(e);}, 600);
        });
        on($(".myshield_item"), "mouseout", function(e) {
            myshield.$domParser.popup.hideTimer = setTimeout(function(e){ myshield.$domParser.popup.hide(false);},500);
            if(myshield.$domParser.popup.showTimer) clearTimeout(myshield.$domParser.popup.showTimer);
        });
        
        var shield = {
            red: "http://mystartshield.com/extensions/blocked.html?url=",
            green: "http://mystartshield.com/extensions/safe.html"
        };

        if (!isSafe) {
            var url = shield.red + link.href;
            link.setAttribute("href", url);
            link.setAttribute("dirtyhref", url);
        }
   },
    popup : {
        $popup : null,
        INJWEBID : "MYSHIELD" ,
        INJWEBSEL : '#MYSHIELD ',
        content : "",
        POPUP_RULES : [
            '.psw_icon {margin:-2px 0 0 15px;padding:0; position: absolute; width:22px !important; height:22px !important; background-size: contain !important; line-height:16px !important;}',
            '{position:absolute; margin: 0 0 0 20px; display:none; z-index:9999999;}',
            '.myshield-tooltip { position: absolute; width: 200px; height: auto; background-color: #a5abb2; color: #fff; border-radius: 4px; box-shadow: 0 0 0 1px rgba(255,255,255,0.8); }',
            '.myshield-tooltip-arrow { right: 100%; top:45px; border: solid transparent; height: 0; width: 0; position: absolute; border-color: rgba(128, 128, 128, 0); border-right-color: #a5abb2; border-width: 7px; }',
                '.myshield-tooltip-text { padding: 12px 15px 15px; line-height: 18px; font-size: 14px; font-weight: 300; text-transform: uppercase; font-family: "Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif; text-align:center;}',
            '.myshield-tooltip-text strong { font-weight: 600; }',
            '.myshield-tooltip-safe { background-color: #5dc56f; }',
            '.myshield-tooltip-safe .myshield-tooltip-arrow { border-right-color: #5dc56f; }',
            '.myshield-tooltip-unsafe { background-color: #C5202F; }',
            '.myshield-tooltip-unsafe .myshield-tooltip-arrow { border-right-color: #C5202F; }',
            '.myshield-tooltip-attention { background-color: #ecd047; color: #282828; }',
            '.myshield-tooltip-attention .myshield-tooltip-arrow { border-right-color: #ecd047; }',
            '.myshield-tooltip-logo {color: #fff;text-decoration: none; font-size:12px; border-bottom: solid 1px rgba(0,0,0,.1); display:block; height:30px; line-height:30px;text-align:center;}',
            '.myshield-tooltip-logo:hover {color: inherit !important;}',
            '.myshield-tooltip-logo-text {vertical-align: top;}',
            '.myshield-tooltip-attention .myshield-tooltip-logo {color:#282828;}'
        ],
        init: function() {
            if($('#' + this.INJWEBID) && $('#' + this.INJWEBID).length > 0) return;
            for (var i = 1; i < this.POPUP_RULES.length; i++) {
                this.POPUP_RULES[i] = this.INJWEBSEL + this.POPUP_RULES[i];
            }
            
            this.content =
            '<div id="pswcontainer" class="myshield-tooltip" style="width:110px;height:80px;">'+
                '<div class="myshield-tooltip-arrow"></div>'+
                '<a href="" class="myshield-tooltip-logo">' +
                    '<span class="myshield-tooltip-logo-img"></span>' +
                    '<span class="myshield-tooltip-logo-text">MyStart Shield</span>' +
                '</a>'+
                '<div id="pswtext" class="myshield-tooltip-text"></div>'+
            '</div>';
            var WEBINJ =
            "<div id='"+ this.INJWEBID+"'>"+
                this.content +
            "</div>";
        
            var POPUP_CSS = this.POPUP_RULES.join("\n");
            var head = document.getElementsByTagName("head")[0];
            popcss = document.createElement("style");
            popcss.type = "text/css";
            popcss.innerHTML = POPUP_CSS;
            head.appendChild(popcss);
            //console.log("adding style");

            this.$popup = document.createElement("div");
            this.$popup.id = this.INJWEBID;
            this.$popup.style.display = 'block';
            this.$popup.style.position = 'absolute';
            document.body.append(this.$popup);
        },
        showTimer : null,
        hideTimer : null,
        hide : function() {
            if(this.hideTimer) clearTimeout(this.hideTimer);
            this.hideTimer = null;
            this.$popup.innerHTML = '';
        },
        show : function(e) {
            this.hide(true);
             // reset contents
            this.$popup.innerHTML = this.content;
            var addClass = ''; removeClasses = '';
            var $a = e.target;
            title = $a.getAttribute("title");
            if (title === "SECURE") {
                addClass = "myshield-tooltip-safe";
                removeClass = "myshield-tooltip-unsafe";
            }else{
                addClass = "myshield-tooltip-unsafe";
                removeClass = "myshield-tooltip-safe";
            }
            $("#pswtext").innerHTML = title;
            this.$popup.firstChild.className += ' ' + addClass;
            this.$popup.firstChild.className = this.$popup.firstChild.className.replace(new RegExp('(^|\\b)' + removeClass.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
            var rect = $a.getBoundingClientRect();
            var offset = {
              'top': rect.top + document.body.scrollTop,
              'left': rect.left + document.body.scrollLeft
            };
            var x = offset.left + 10;
            var y = offset.top - 40;
            this.$popup.style.left = x + "px";
            this.$popup.style.top = y + "px";
        }
    }
};

myshield.$domParser.init();
