function onMessageFromBG (request, sender, sendResponse){
	if (!request.message ) return;
	switch(request.message){
        case 'optionsresponse':
            console.log("optionsresponse:" + JSON.stringify(request));
            options = request.options;
            antiphishing = options.antiphishing;
            popup = options.popup;
        	alexa = options.alexa;
            google = options.search.google;
            yahoo = options.search.yahoo;
            bing = options.search.bing;
            ad = options.tracking.$ad;
            analytics = options.tracking.$analytics;
            social = options.tracking.$social;
            sponsors = options.tracking.$sponsors;
           
            if (antiphishing === 1)
                $("#antiphishing").addClass("checkbox-checked");
        	if (alexa === 1) 
        	    $("#alexa").addClass("checkbox-checked");
            if (google === 1)
                $("#google").addClass("checkbox-checked");
            if (yahoo === 1)
                $("#yahoo").addClass("checkbox-checked");
            if (bing === 1)
                $("#bing").addClass("checkbox-checked");
            if (ad === 1)
                $("#ad").addClass("checkbox-checked");
            if (analytics === 1)
                $("#analytics").addClass("checkbox-checked");
            if (social === 1)
                $("#social").addClass("checkbox-checked");
            if (sponsors === 1)
                $("#sponsors").addClass("checkbox-checked");
            if (popup === 1)
                $("#popup").addClass("checkbox-checked");
    
        break;
	}
}

function onMessageFromDOM (event) {	
	if (!event.data.message) return;
	console.log("onMessageFromDOM:" + JSON.stringify(event.data.message));
    chrome.runtime.sendMessage(event.data);
}

function onLoad() {
	console.log("onload starts");
    
    //window.parent.postMessage({message: "trackpage", title: "Settings", page: "/options.html"}, '*');
    window.addEventListener("contextmenu", function(e) { e.preventDefault(); });
	
    $(".checkbox").unbind().click(function(e) {
        target = $(e.target);
        if (target.attr('class').indexOf("checkbox-checked") === -1){
            target.addClass("checkbox-checked");
        }else{
            target.removeClass("checkbox-checked");
        }
         $("#btn-save").click();
    });
    
    $('.item').unbind().click(function(e) {
        $('#btn-save').addClass('blue');
    });
    
    
    $("#btn-save").unbind().click(function(e) {
        console.log("send message done");
		var antiphishing = ($("#antiphishing").attr('class').indexOf("checkbox-checked") === -1)? 0 : 1; 
		var popup = ($("#popup").attr('class').indexOf("checkbox-checked") === -1)? 0 : 1; 
        var alexa = $("#alexa").attr("class").indexOf("checkbox-checked") === -1 ? 0 : 1;
		var google = ($("#google").attr('class').indexOf("checkbox-checked") === -1)? 0 : 1; 
		var yahoo = ($("#yahoo").attr('class').indexOf("checkbox-checked") === -1)? 0 : 1; 
		var bing = ($("#bing").attr('class').indexOf("checkbox-checked") === -1)? 0 : 1; 
		var ad = ($("#ad").attr('class').indexOf("checkbox-checked") === -1)? 0 : 1; 
		var analytics = ($("#analytics").attr('class').indexOf("checkbox-checked") === -1)? 0 : 1; 
		var social = ($("#social").attr('class').indexOf("checkbox-checked") === -1)? 0 : 1; 
		var sponsors = ($("#sponsors").attr('class').indexOf("checkbox-checked") === -1)? 0 : 1; 
        var blockall = (social === 1 && ad ===1 && analytics ===1 && sponsors === 1)? 1 : 0; 
		
		chrome.tabs.getCurrent(function(tab) {
            var  options =  {
                alexa: alexa,
                antiphishing : antiphishing ,
                blockall: blockall,
                popup: popup,
                search : {
                    google : google,
                    yahoo : yahoo,
                    bing : bing
                },
                tracking : {
                    $ad: ad,
                    $analytics: analytics,
                    $social: social,
                    $sponsors: sponsors,
                    $spyware: antiphishing
               }
            };

 		    window.parent.postMessage({message: "savesettings", options: options}, '*');
  		    window.location.reload(false); 
        });    
    }); 
    
    $("#btn-reset").unbind().click(function(e) {
        var options =  {
                       alexa : 1,
                       antiphishing : 1,
                       blockall: 0,
                       popup: 1,
                       search : {
                            google : 1,
                            yahoo : 1,
                            bing : 1
                        },
                        tracking : {
                            $ad: 0,
                            $analytics: 0,
                            $social:0,
                            $sponsors:0,
                            $spyware:1
                        }
                    };
  		window.parent.postMessage({message: "savesettings", options: options}, '*');
  		window.location.reload(false); 
    }); 
       
	chrome.tabs.getSelected(null, function (request){
 		window.parent.postMessage({message: "getoptions", tab: request.id}, '*');
    });
    
    chrome.extension.onMessage.removeListener(onMessageFromBG);
    chrome.extension.onMessage.addListener(onMessageFromBG);
 	
 	window.removeEventListener("message", onMessageFromDOM, false);
	window.addEventListener("message", onMessageFromDOM, false);
	
	console.log("onload ends");
}

document.addEventListener("DOMContentLoaded", onLoad, false);
