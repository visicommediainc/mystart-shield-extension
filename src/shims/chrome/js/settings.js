if (!myshield)
    var myshield = {};
myshield.$settings = {
    options : {
        alexa: 1,
        antiphishing : 1,
        popup: 1,
        blockall: 0,
        search : {
            google : 1,
            yahoo : 1,
            bing : 1
        },
        tracking : {
            $ad: 0,
            $analytics: 0,
            $social:0,
            $sponsors:0,
            $spyware:1
       }
    },
    setOptions : function(options) {
        myshield.$settings.options = options;
 	    chrome.storage.local.set({"options": myshield.$settings.options}, function(){});
    }
};

chrome.storage.local.get("blockall", function(result){
	try{
		myshield.$settings.options.blockall = result.blockall  ;
		if (typeof myshield.$settings.options.blockall == "undefined"){
		    myshield.$settings.options.blockall = 0;
		    chrome.storage.local.set({"blockall": myshield.$settings.options.blockall}, function(){});
		}
		//console.log("settings.blockall:" + myshield.$settings.options.blockall);
	}catch(ex){
		myshield.$settings.options.blockall = 0;
	}
});
chrome.storage.local.get("options", function(result){
	try{
		if (typeof result.options == "undefined"){
		    chrome.storage.local.set({"options": myshield.$settings.options}, function(){});
		}else{
			myshield.$settings.options = result.options  ;
        }
	}catch(ex){
	}
});

