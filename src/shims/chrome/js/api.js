if (!myshield)
    var myshield = {};

myshield.$api = {
    baseurl: "http://urlfilter2.vmn.net/safeweb/tracker/panda/tracker_min.json", //"http://urlfilter2.vmn.net/safeweb/tracker/panda/tracker.js.br", 
    trackerLastUpdate: 0,
    beatLastUpdate: 0,
    updateInterval: 25 * 60 * 1000, // 25 mins
    beatInterval: 24 * 60 * 60 * 1000, // 1 day
    trackers: [],
    tracker: {},
    ignore_urls: [
        "chrome://",
        "chrome://extensions",
        "chrome-extension:",
        "chrome-search://",
        "chrome-devtools://",
        "chrome.google.com",
        "about:",
        "view-source:",
        "file://",
        "//localhost"
    ],
    version: chrome.app.getDetails().version,

    getValue: function(value, cb) {
        chrome.storage.local.get(value, cb);
    },
    setValue: function(json) {
        chrome.storage.local.set(json);
    },
    cleanUrl: function(url) {
        if (url.indexOf("chrome://") !== -1 || url.indexOf("file://") !== -1)
            return url;
        var match = url.match(/:\/\/(www[0-9]?\.)?(.[^/:]+)/i);
        if (match !== null && match.length > 2 && typeof match[2] === 'string' && match[2].length > 0) {
            if (url.indexOf("download.pandasecurity.com") !== -1) {
                blockUrl = url.split("url=")[1];
                if (typeof blockUrl === "undefined")
                    return match[2];

                return blockUrl;
            } else {
                return match[2];
            }
        } else {
            return null;
        }
    },
    checkUrl: function(url) {
        if (!url || url === "") return false;
        for (var i = 0, j = myshield.$api.ignore_urls.length; i < j; i++) {
            if (url.indexOf(myshield.$api.ignore_urls[i]) > -1) return false;
        }
        return true;
    },
    getTrack: function(url) {
        for (i = 0; i < myshield.$api.trackers.length; i++) {
            if (myshield.$api.trackers[i].url === url) {
                return myshield.$api.trackers[i];
            }
        }
    },
    removeTrack: function(url) {
        for (i = 0; i < myshield.$api.trackers.length; i++) {
            if (myshield.$api.trackers[i].url === url) {
                myshield.$api.trackers.splice(i, 1);
                return;
            }
        }
    }
};

(function() {
    if (chrome.storage) {
        chrome.storage.local.get("tracker", function(result) {
            try {
                myshield.$api.tracker = JSON.parse(result.tracker) || {};
            } catch (ex) {
                //console.log("settings.error:" + ex.message);
                myshield.$api.tracker = {};
            }
        });


        chrome.storage.local.get("trackers", function(result) {
            try {
                myshield.$api.trackers = JSON.parse(result.trackers) || [];
            } catch (ex) {
                myshield.$api.trackers = [];
            }
        });
    }
})();